
/*
'===============================================================================
'  Generated From - CSharp_dOOdads_BusinessEntity.vbgen
' 
'  ** IMPORTANT  ** 
'  How to Generate your stored procedures:
' 
'  SQL        = SQL_StoredProcs.vbgen
'  ACCESS     = Access_StoredProcs.vbgen
'  ORACLE     = Oracle_StoredProcs.vbgen
'  FIREBIRD   = FirebirdStoredProcs.vbgen
'  POSTGRESQL = PostgreSQL_StoredProcs.vbgen
'
'  The supporting base class SqlClientEntity is in the Architecture directory in "dOOdads".
'  
'  This object is 'abstract' which means you need to inherit from it to be able
'  to instantiate it.  This is very easilly done. You can override properties and
'  methods in your derived class, this allows you to regenerate this class at any
'  time and not worry about overwriting custom code. 
'
'  NEVER EDIT THIS FILE.
'
'  public class YourObject :  _YourObject
'  {
'
'  }
'
'===============================================================================
*/

// Generated by MyGeneration Version # (1.3.0.3)

using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.Collections.Specialized;

using MyGeneration.dOOdads;

namespace BusinessLayer
{
	public abstract class _tblProducerxMovie : SqlClientEntity
	{
		public _tblProducerxMovie()
		{
			this.QuerySource = "tblProducerxMovie";
			this.MappingName = "tblProducerxMovie";

		}	

		//=================================================================
		//  public Overrides void AddNew()
		//=================================================================
		//
		//=================================================================
		public override void AddNew()
		{
			base.AddNew();
			
		}
		
		
		public override void FlushData()
		{
			this._whereClause = null;
			this._aggregateClause = null;
			base.FlushData();
		}
		
		//=================================================================
		//  	public Function LoadAll() As Boolean
		//=================================================================
		//  Loads all of the records in the database, and sets the currentRow to the first row
		//=================================================================
		public bool LoadAll() 
		{
			ListDictionary parameters = null;
			
			return base.LoadFromSql("[" + this.SchemaStoredProcedure + "proc_tblProducerxMovieLoadAll]", parameters);
		}
	
		//=================================================================
		// public Overridable Function LoadByPrimaryKey()  As Boolean
		//=================================================================
		//  Loads a single row of via the primary key
		//=================================================================
		public virtual bool LoadByPrimaryKey(int Producer_movie_id_pk)
		{
			ListDictionary parameters = new ListDictionary();
			parameters.Add(Parameters.Producer_movie_id_pk, Producer_movie_id_pk);

		
			return base.LoadFromSql("[" + this.SchemaStoredProcedure + "proc_tblProducerxMovieLoadByPrimaryKey]", parameters);
		}
		
		#region Parameters
		protected class Parameters
		{
			
			public static SqlParameter Producer_movie_id_pk
			{
				get
				{
					return new SqlParameter("@Producer_movie_id_pk", SqlDbType.Int, 0);
				}
			}
			
			public static SqlParameter Producer_id_fk
			{
				get
				{
					return new SqlParameter("@Producer_id_fk", SqlDbType.Int, 0);
				}
			}
			
			public static SqlParameter Movie_id_fk
			{
				get
				{
					return new SqlParameter("@Movie_id_fk", SqlDbType.Int, 0);
				}
			}
			
		}
		#endregion		
	
		#region ColumnNames
		public class ColumnNames
		{  
            public const string Producer_movie_id_pk = "producer_movie_id_pk";
            public const string Producer_id_fk = "producer_id_fk";
            public const string Movie_id_fk = "movie_id_fk";

			static public string ToPropertyName(string columnName)
			{
				if(ht == null)
				{
					ht = new Hashtable();
					
					ht[Producer_movie_id_pk] = _tblProducerxMovie.PropertyNames.Producer_movie_id_pk;
					ht[Producer_id_fk] = _tblProducerxMovie.PropertyNames.Producer_id_fk;
					ht[Movie_id_fk] = _tblProducerxMovie.PropertyNames.Movie_id_fk;

				}
				return (string)ht[columnName];
			}

			static private Hashtable ht = null;			 
		}
		#endregion
		
		#region PropertyNames
		public class PropertyNames
		{  
            public const string Producer_movie_id_pk = "Producer_movie_id_pk";
            public const string Producer_id_fk = "Producer_id_fk";
            public const string Movie_id_fk = "Movie_id_fk";

			static public string ToColumnName(string propertyName)
			{
				if(ht == null)
				{
					ht = new Hashtable();
					
					ht[Producer_movie_id_pk] = _tblProducerxMovie.ColumnNames.Producer_movie_id_pk;
					ht[Producer_id_fk] = _tblProducerxMovie.ColumnNames.Producer_id_fk;
					ht[Movie_id_fk] = _tblProducerxMovie.ColumnNames.Movie_id_fk;

				}
				return (string)ht[propertyName];
			}

			static private Hashtable ht = null;			 
		}			 
		#endregion	

		#region StringPropertyNames
		public class StringPropertyNames
		{  
            public const string Producer_movie_id_pk = "s_Producer_movie_id_pk";
            public const string Producer_id_fk = "s_Producer_id_fk";
            public const string Movie_id_fk = "s_Movie_id_fk";

		}
		#endregion		
		
		#region Properties
	
		public virtual int Producer_movie_id_pk
	    {
			get
	        {
				return base.Getint(ColumnNames.Producer_movie_id_pk);
			}
			set
	        {
				base.Setint(ColumnNames.Producer_movie_id_pk, value);
			}
		}

		public virtual int Producer_id_fk
	    {
			get
	        {
				return base.Getint(ColumnNames.Producer_id_fk);
			}
			set
	        {
				base.Setint(ColumnNames.Producer_id_fk, value);
			}
		}

		public virtual int Movie_id_fk
	    {
			get
	        {
				return base.Getint(ColumnNames.Movie_id_fk);
			}
			set
	        {
				base.Setint(ColumnNames.Movie_id_fk, value);
			}
		}


		#endregion
		
		#region String Properties
	
		public virtual string s_Producer_movie_id_pk
	    {
			get
	        {
				return this.IsColumnNull(ColumnNames.Producer_movie_id_pk) ? string.Empty : base.GetintAsString(ColumnNames.Producer_movie_id_pk);
			}
			set
	        {
				if(string.Empty == value)
					this.SetColumnNull(ColumnNames.Producer_movie_id_pk);
				else
					this.Producer_movie_id_pk = base.SetintAsString(ColumnNames.Producer_movie_id_pk, value);
			}
		}

		public virtual string s_Producer_id_fk
	    {
			get
	        {
				return this.IsColumnNull(ColumnNames.Producer_id_fk) ? string.Empty : base.GetintAsString(ColumnNames.Producer_id_fk);
			}
			set
	        {
				if(string.Empty == value)
					this.SetColumnNull(ColumnNames.Producer_id_fk);
				else
					this.Producer_id_fk = base.SetintAsString(ColumnNames.Producer_id_fk, value);
			}
		}

		public virtual string s_Movie_id_fk
	    {
			get
	        {
				return this.IsColumnNull(ColumnNames.Movie_id_fk) ? string.Empty : base.GetintAsString(ColumnNames.Movie_id_fk);
			}
			set
	        {
				if(string.Empty == value)
					this.SetColumnNull(ColumnNames.Movie_id_fk);
				else
					this.Movie_id_fk = base.SetintAsString(ColumnNames.Movie_id_fk, value);
			}
		}


		#endregion		
	
		#region Where Clause
		public class WhereClause
		{
			public WhereClause(BusinessEntity entity)
			{
				this._entity = entity;
			}
			
			public TearOffWhereParameter TearOff
			{
				get
				{
					if(_tearOff == null)
					{
						_tearOff = new TearOffWhereParameter(this);
					}

					return _tearOff;
				}
			}

			#region WhereParameter TearOff's
			public class TearOffWhereParameter
			{
				public TearOffWhereParameter(WhereClause clause)
				{
					this._clause = clause;
				}
				
				
				public WhereParameter Producer_movie_id_pk
				{
					get
					{
							WhereParameter where = new WhereParameter(ColumnNames.Producer_movie_id_pk, Parameters.Producer_movie_id_pk);
							this._clause._entity.Query.AddWhereParameter(where);
							return where;
					}
				}

				public WhereParameter Producer_id_fk
				{
					get
					{
							WhereParameter where = new WhereParameter(ColumnNames.Producer_id_fk, Parameters.Producer_id_fk);
							this._clause._entity.Query.AddWhereParameter(where);
							return where;
					}
				}

				public WhereParameter Movie_id_fk
				{
					get
					{
							WhereParameter where = new WhereParameter(ColumnNames.Movie_id_fk, Parameters.Movie_id_fk);
							this._clause._entity.Query.AddWhereParameter(where);
							return where;
					}
				}


				private WhereClause _clause;
			}
			#endregion
		
			public WhereParameter Producer_movie_id_pk
		    {
				get
		        {
					if(_Producer_movie_id_pk_W == null)
	        	    {
						_Producer_movie_id_pk_W = TearOff.Producer_movie_id_pk;
					}
					return _Producer_movie_id_pk_W;
				}
			}

			public WhereParameter Producer_id_fk
		    {
				get
		        {
					if(_Producer_id_fk_W == null)
	        	    {
						_Producer_id_fk_W = TearOff.Producer_id_fk;
					}
					return _Producer_id_fk_W;
				}
			}

			public WhereParameter Movie_id_fk
		    {
				get
		        {
					if(_Movie_id_fk_W == null)
	        	    {
						_Movie_id_fk_W = TearOff.Movie_id_fk;
					}
					return _Movie_id_fk_W;
				}
			}

			private WhereParameter _Producer_movie_id_pk_W = null;
			private WhereParameter _Producer_id_fk_W = null;
			private WhereParameter _Movie_id_fk_W = null;

			public void WhereClauseReset()
			{
				_Producer_movie_id_pk_W = null;
				_Producer_id_fk_W = null;
				_Movie_id_fk_W = null;

				this._entity.Query.FlushWhereParameters();

			}
	
			private BusinessEntity _entity;
			private TearOffWhereParameter _tearOff;
			
		}
	
		public WhereClause Where
		{
			get
			{
				if(_whereClause == null)
				{
					_whereClause = new WhereClause(this);
				}
		
				return _whereClause;
			}
		}
		
		private WhereClause _whereClause = null;	
		#endregion
	
		#region Aggregate Clause
		public class AggregateClause
		{
			public AggregateClause(BusinessEntity entity)
			{
				this._entity = entity;
			}
			
			public TearOffAggregateParameter TearOff
			{
				get
				{
					if(_tearOff == null)
					{
						_tearOff = new TearOffAggregateParameter(this);
					}

					return _tearOff;
				}
			}

			#region AggregateParameter TearOff's
			public class TearOffAggregateParameter
			{
				public TearOffAggregateParameter(AggregateClause clause)
				{
					this._clause = clause;
				}
				
				
				public AggregateParameter Producer_movie_id_pk
				{
					get
					{
							AggregateParameter aggregate = new AggregateParameter(ColumnNames.Producer_movie_id_pk, Parameters.Producer_movie_id_pk);
							this._clause._entity.Query.AddAggregateParameter(aggregate);
							return aggregate;
					}
				}

				public AggregateParameter Producer_id_fk
				{
					get
					{
							AggregateParameter aggregate = new AggregateParameter(ColumnNames.Producer_id_fk, Parameters.Producer_id_fk);
							this._clause._entity.Query.AddAggregateParameter(aggregate);
							return aggregate;
					}
				}

				public AggregateParameter Movie_id_fk
				{
					get
					{
							AggregateParameter aggregate = new AggregateParameter(ColumnNames.Movie_id_fk, Parameters.Movie_id_fk);
							this._clause._entity.Query.AddAggregateParameter(aggregate);
							return aggregate;
					}
				}


				private AggregateClause _clause;
			}
			#endregion
		
			public AggregateParameter Producer_movie_id_pk
		    {
				get
		        {
					if(_Producer_movie_id_pk_W == null)
	        	    {
						_Producer_movie_id_pk_W = TearOff.Producer_movie_id_pk;
					}
					return _Producer_movie_id_pk_W;
				}
			}

			public AggregateParameter Producer_id_fk
		    {
				get
		        {
					if(_Producer_id_fk_W == null)
	        	    {
						_Producer_id_fk_W = TearOff.Producer_id_fk;
					}
					return _Producer_id_fk_W;
				}
			}

			public AggregateParameter Movie_id_fk
		    {
				get
		        {
					if(_Movie_id_fk_W == null)
	        	    {
						_Movie_id_fk_W = TearOff.Movie_id_fk;
					}
					return _Movie_id_fk_W;
				}
			}

			private AggregateParameter _Producer_movie_id_pk_W = null;
			private AggregateParameter _Producer_id_fk_W = null;
			private AggregateParameter _Movie_id_fk_W = null;

			public void AggregateClauseReset()
			{
				_Producer_movie_id_pk_W = null;
				_Producer_id_fk_W = null;
				_Movie_id_fk_W = null;

				this._entity.Query.FlushAggregateParameters();

			}
	
			private BusinessEntity _entity;
			private TearOffAggregateParameter _tearOff;
			
		}
	
		public AggregateClause Aggregate
		{
			get
			{
				if(_aggregateClause == null)
				{
					_aggregateClause = new AggregateClause(this);
				}
		
				return _aggregateClause;
			}
		}
		
		private AggregateClause _aggregateClause = null;	
		#endregion
	
		protected override IDbCommand GetInsertCommand() 
		{
		
			SqlCommand cmd = new SqlCommand();
			cmd.CommandType = CommandType.StoredProcedure;
			cmd.CommandText = "[" + this.SchemaStoredProcedure + "proc_tblProducerxMovieInsert]";
	
			CreateParameters(cmd);
			
			SqlParameter p;
			p = cmd.Parameters[Parameters.Producer_movie_id_pk.ParameterName];
			p.Direction = ParameterDirection.Output;
    
			return cmd;
		}
	
		protected override IDbCommand GetUpdateCommand()
		{
		
			SqlCommand cmd = new SqlCommand();
			cmd.CommandType = CommandType.StoredProcedure;
			cmd.CommandText = "[" + this.SchemaStoredProcedure + "proc_tblProducerxMovieUpdate]";
	
			CreateParameters(cmd);
			      
			return cmd;
		}
	
		protected override IDbCommand GetDeleteCommand()
		{
		
			SqlCommand cmd = new SqlCommand();
			cmd.CommandType = CommandType.StoredProcedure;
			cmd.CommandText = "[" + this.SchemaStoredProcedure + "proc_tblProducerxMovieDelete]";
	
			SqlParameter p;
			p = cmd.Parameters.Add(Parameters.Producer_movie_id_pk);
			p.SourceColumn = ColumnNames.Producer_movie_id_pk;
			p.SourceVersion = DataRowVersion.Current;

  
			return cmd;
		}
		
		private IDbCommand CreateParameters(SqlCommand cmd)
		{
			SqlParameter p;
		
			p = cmd.Parameters.Add(Parameters.Producer_movie_id_pk);
			p.SourceColumn = ColumnNames.Producer_movie_id_pk;
			p.SourceVersion = DataRowVersion.Current;

			p = cmd.Parameters.Add(Parameters.Producer_id_fk);
			p.SourceColumn = ColumnNames.Producer_id_fk;
			p.SourceVersion = DataRowVersion.Current;

			p = cmd.Parameters.Add(Parameters.Movie_id_fk);
			p.SourceColumn = ColumnNames.Movie_id_fk;
			p.SourceVersion = DataRowVersion.Current;


			return cmd;
		}
	}
}
