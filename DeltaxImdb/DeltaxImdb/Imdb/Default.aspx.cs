﻿using BusinessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    [WebMethod]
    public static String getAllActors()
    {
        tblActor objActor = new tblActor();
        return objActor.getAllActor();
    }

    [WebMethod]
    public static string delActor(int data) {
        tblActor objActor = new tblActor();
        objActor.LoadByPrimaryKey(data);
        objActor.MarkAsDeleted();
        objActor.Save();

        return getAllActors();
    }

    [WebMethod]
    public static string editActor(int data)
    {
        tblActor objActor = new tblActor();
        return objActor.getActorDetail(data);
    }

    protected void btnsubmit_Click(object sender, EventArgs e)
    {
        tblActor objActor = new tblActor();

        if (hdnactoridpk.Value == "")
        {
            objActor.AddNew();
        }
        else {
            objActor.LoadByPrimaryKey(Convert.ToInt32(hdnactoridpk.Value));
        }

        objActor.Actor_name = txtActorName.Text;
        if (rdbmale.Checked)
            objActor.Actor_sex = rdbmale.Text;
        else if (rdbfemale.Checked)
            objActor.Actor_sex = rdbfemale.Text;
        objActor.Actor_bio = txtBio.Text;
        objActor.Actor_dob = caldob.SelectedDate.ToString();

        objActor.Save();

        txtActorName.Text = "";
        txtBio.Text = "";
    }
}