﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Movie.aspx.cs" Inherits="Movie" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script src="jquery-3.1.0.js"></script>
    <script src="jquery.tmpl.js"></script>
    <script id="data" type="text/x-jquery-tmpl">
        <tr>
            <td>${movie_name}</td>
            <td>${movie_release_year}</td>
            <td>${movie_plot}</td>
            <td><img src="${movie_poster_image}" style="height:50px; width:50px;" /></td>
            <td>
                <a id="${movie_id_pk}del" href="" >Delete</a> | <a href="MovieDetail.aspx?id=${movie_id_pk}" >Edit<//a>
            </td>
        </tr>
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            ajax("", "", "");

            $(document).on("click", "[id$=del]", function (e) {
                e.preventDefault();
                if (confirm("Are you Sure? you want to delete")) {
                    ajax(parseInt($(this).attr("id")), "del", "delMovie");
                }
            });
        });

        function ajax(data, method, func) {
            func = func == "" ? "getAllMovies" : func;
            data = JSON.stringify({ data: data });
            $.ajax({
                type: "post",
                contentType: "application/json; charset=utf-8",
                url: "Movie.aspx/"+func,
                data: data,
                dataType: "json",
                success: function (response) {
                     filldata($.parseJSON(response.d));
                },
                failure: function (response) {
                    alert(response.d);
                },
                error: function (response) {
                    alert(response);
                }
            });
        }

        function filldata(str){
            $("#tbl > tbody").empty();
            $("#data").tmpl(str).appendTo("#tbl");
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table id="tbl" style="text-align:center; border:1px solid black" align="center">
            <thead  style="background-color:cyan">
                <tr>
                    <td>Movie Name</td>
                    <td>Release Year</td>
                    <td>Plot</td>
                    <td>Poster</td>
                    <td>Action</td>
                </tr>
            </thead>
            <tbody>

            </tbody>
        </table>
    </div>
    </form>
</body>
</html>
