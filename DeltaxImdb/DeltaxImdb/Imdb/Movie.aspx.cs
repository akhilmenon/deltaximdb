﻿using BusinessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Movie : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    [WebMethod]
    public static String getAllMovies()
    {
        tblMovie objMovie = new tblMovie();
        return objMovie.getAllMovie();
    }

    [WebMethod]
    public static string delMovie(int data)
    {
        tblMovie objMovie = new tblMovie();
        objMovie.LoadByPrimaryKey(data);
        objMovie.MarkAsDeleted();
        objMovie.Save();

        tblActorxMovie objActorxMovie = new tblActorxMovie();
        objActorxMovie.delAllActors(data);


        return getAllMovies();
    }
}