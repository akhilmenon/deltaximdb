﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="MovieDetail.aspx.cs" Inherits="MovieDetail" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script src="jquery-3.1.0.js"></script>
    <script src="jquery.tmpl.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            ajax("", "", "");

            if ($("#txtProducerName").val() != "") {
                $("#producer").css("display", "block");
            }

            if ($("#txtActorName").val() != "") {
                $("#actor").css("display", "block");
            }

            $(document).on("click", "[id$=del]", function () {
                if (confirm("Are you Sure? you want to delete")) {
                    ajax(parseInt($(this).attr("id")), "del", "delMovie");
                }
            });

            $(document).on("click", "[id$=edit]", function () {
                 ajax(parseInt($(this).attr("id")), "edit", "editMovie");
            });

            $(document).on("click", "[id$=addactor]", function (e) {
                e.preventDefault();
                $("#actor").css("display", "block");
                $("#producer").css("display", "none");
            });

            $(document).on("click", "[id$=addproducer]", function (e) {
                e.preventDefault();
                $("#producer").css("display", "block");
                $("#actor").css("display", "none");
            });

        });

        function ajax(data, method, func) {
            func = func == "" ? "getAllMovies" : func;
            data = JSON.stringify({ data: data });
            $.ajax({
                type: "post",
                contentType: "application/json; charset=utf-8",
                url: "Movie.aspx/"+func,
                data: data,
                dataType: "json",
                success: function (response) {
                    if (method == "edit") {
                        str = $.parseJSON(response.d)
                        $("#hdnmovieidpk").val(str[0].producer_id_pk);
                        $("#txtMovieName").val(str[0].producer_name);
                    }
                    else {
                        filldata($.parseJSON(response.d));
                    }
                },
                failure: function (response) {
                    alert(response.d);
                },
                error: function (response) {
                    alert(response);
                }
            });
        }

        function filldata(str){
            $("#tbl > tbody").empty();
            $("#data").tmpl(str).appendTo("#tbl");
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <div>
            <a id="addactor" href="" >Add Actor</a> | <a id="addproducer" href="">Add Producer</a>
        </div>
        <div>
            <asp:TextBox ID="txtMovieName" runat="server" placeholder="Enter Movie Name"></asp:TextBox>
        </div>
        <div>
            <asp:TextBox ID="txtReleaseYear" runat="server" placeholder="Release Year"></asp:TextBox>
        </div>
        <div>
            <asp:TextBox ID="txtPlot" TextMode="MultiLine" runat="server" placeholder="Enter Plot"></asp:TextBox>
        </div>
        <div>
            <asp:Label Text="Actors" runat="server"></asp:Label>
            <asp:CheckBoxList ID="actors" runat="server"></asp:CheckBoxList>
        </div>
        <div>
            <asp:Label Text="Producers" runat="server"></asp:Label>
            <asp:CheckBoxList ID="producers" runat="server"></asp:CheckBoxList>
        </div>
        <div>
            <asp:FileUpload ID="fileupload" runat="server" />
        </div>
        <div>
            <asp:Button ID="btnsubmit" Text="Submit" runat="server" OnClick="btnsubmit_Click" />
            <asp:HiddenField ID="hdnmovieidpk" Value="" runat="server" />
        </div>
        <div id="actor" style="display:none;">
           Add Actor
            <div>
            <asp:TextBox ID="txtActorName" runat="server" placeholder="Enter Actor Name"></asp:TextBox>
        </div>
        <div>
            <asp:RadioButton ID="RadioButton1" Text="Male" GroupName="gender" runat="server" />
            <asp:RadioButton ID="RadioButton2" Text="Female" GroupName="gender" runat="server" />
        </div>
        <div>
            <asp:Calendar ID="Calendar1" runat="server" placeholder="Enter Dob"></asp:Calendar>
        </div>
        <div>
            <asp:TextBox ID="TextBox1" TextMode="MultiLine" runat="server" placeholder="Enter Bio"></asp:TextBox>
        </div>
        <div>
            <asp:Button ID="btnactorsubmit" Text="Submit" runat="server" OnClick="btnactorsubmit_Click" />
            <asp:HiddenField ID="hdnactoridpk" Value="" runat="server" />
        </div>
        </div>
        <div id="producer" style="display:none;">
           Add Producer
            <div>
                <asp:TextBox ID="txtProducerName" runat="server" placeholder="Enter Producer Name"></asp:TextBox>
            </div>
            <div>
                <asp:RadioButton ID="rdbmale" Text="Male" GroupName="gender" runat="server" />
                <asp:RadioButton ID="rdbfemale" Text="Female" GroupName="gender" runat="server" />
            </div>
            <div>
                <asp:Calendar ID="caldob" runat="server" placeholder="Enter Dob"></asp:Calendar>
            </div>
            <div>
                <asp:TextBox ID="txtBio" TextMode="MultiLine" runat="server" placeholder="Enter Bio"></asp:TextBox>
            </div>
            <div>
                <asp:Button ID="btnprodsubmit" Text="Submit" runat="server" OnClick="btnprodsubmit_Click" />
                <asp:HiddenField ID="hdnproduceridpk" Value="" runat="server" />
            </div>
        </div>
    </div>
    </form>
</body>
</html>
