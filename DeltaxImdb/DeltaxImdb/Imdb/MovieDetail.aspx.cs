﻿using BusinessLayer;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class MovieDetail : System.Web.UI.Page
{
    
    protected void Page_Load(object sender, EventArgs e)
    {
        hdnmovieidpk.Value = Request["id"];
        if (!Page.IsPostBack) { 
            getAllActorsProducers();
            if(hdnmovieidpk.Value != "")
                editMovie(Convert.ToInt32(hdnmovieidpk.Value));
        }
    }

    public void editMovie(int data)
    {
        tblMovie objMovie = new tblMovie();
        DataTable dt = objMovie.getMoviesDetail(data);

        foreach (DataRow dr in dt.Rows) {
            txtMovieName.Text = dr["movie_name"].ToString();
            txtReleaseYear.Text = dr["movie_release_year"].ToString();
            txtPlot.Text = dr["movie_plot"].ToString();

            foreach (ListItem listitem in actors.Items)
            {
                if (listitem.Value == dr["actor_id_fk"].ToString())
                {
                    listitem.Selected = true;
                }
            }

            foreach (ListItem listitem in producers.Items)
            {
                if (listitem.Value == dr["producer_id_fk"].ToString())
                {
                    listitem.Selected = true;
                }
            }
        }

    }

    public void getAllActorsProducers() {
        tblActor objActor = new tblActor();
        actors.DataSource = objActor.getAllActors();
        actors.DataTextField = "actor_name";
        actors.DataValueField = "actor_id_pk"; 
        actors.DataBind();

        tblProducer objProducer = new tblProducer();
        producers.DataSource = objProducer.getAllProducers();
        producers.DataTextField = "producer_name";
        producers.DataValueField = "producer_id_pk";
        producers.DataBind();
    }

    protected void btnsubmit_Click(object sender, EventArgs e)
    {
        tblMovie objMovie = new tblMovie();

        if (hdnmovieidpk.Value == "")
        {
            objMovie.AddNew();
        }
        else
        {
            objMovie.LoadByPrimaryKey(Convert.ToInt32(hdnmovieidpk.Value));
        }

        objMovie.Movie_name = txtMovieName.Text;
        objMovie.Movie_release_year = txtReleaseYear.Text;
        objMovie.Movie_plot = txtPlot.Text;

        String filename = "";
        if (fileupload.HasFile) {
            filename = Path.GetFileName(fileupload.FileName);
            fileupload.SaveAs(Server.MapPath("~/") + filename);
        }

        objMovie.Movie_poster_image = filename;

        objMovie.Save();

        int id = objMovie.Movie_id_pk;
        
        foreach(ListItem listitem in actors.Items){
            if (listitem.Selected) {
                tblActorxMovie objActoxrMovie = new tblActorxMovie();
                objActoxrMovie.AddNew();
                objActoxrMovie.Actor_id_fk = Convert.ToInt32(listitem.Value);
                objActoxrMovie.Movie_id_fk = id;
                objActoxrMovie.Save();
            }
        }

        foreach (ListItem listitem in producers.Items)
        {
            if (listitem.Selected)
            {
                tblProducerxMovie objProducerxrMovie = new tblProducerxMovie();
                objProducerxrMovie.AddNew();
                objProducerxrMovie.Producer_id_fk = Convert.ToInt32(listitem.Value);
                objProducerxrMovie.Movie_id_fk = id;
                objProducerxrMovie.Save();
            }
        }

        txtMovieName.Text = "";
        txtReleaseYear.Text = "";
        txtPlot.Text = "";
    }
    protected void btnprodsubmit_Click(object sender, EventArgs e)
    {
        tblProducer objProducer = new tblProducer();

        if (hdnproduceridpk.Value == "")
        {
            objProducer.AddNew();
        }
        else
        {
            objProducer.LoadByPrimaryKey(Convert.ToInt32(hdnproduceridpk.Value));
        }

        objProducer.Producer_name = txtProducerName.Text;
        if (rdbmale.Checked)
            objProducer.Producer_sex = rdbmale.Text;
        else if (rdbfemale.Checked)
            objProducer.Producer_sex = rdbfemale.Text;
        objProducer.Producer_bio = txtBio.Text;
        objProducer.Producer_dob = caldob.SelectedDate.ToString();

        objProducer.Save();

        txtProducerName.Text = "";
        txtBio.Text = "";
        getAllActorsProducers();
    }
    protected void btnactorsubmit_Click(object sender, EventArgs e)
    {
        tblActor objActor = new tblActor();

        if (hdnactoridpk.Value == "")
        {
            objActor.AddNew();
        }
        else
        {
            objActor.LoadByPrimaryKey(Convert.ToInt32(hdnactoridpk.Value));
        }

        objActor.Actor_name = txtActorName.Text;
        if (rdbmale.Checked)
            objActor.Actor_sex = rdbmale.Text;
        else if (rdbfemale.Checked)
            objActor.Actor_sex = rdbfemale.Text;
        objActor.Actor_bio = txtBio.Text;
        objActor.Actor_dob = caldob.SelectedDate.ToString();

        objActor.Save();

        txtActorName.Text = "";
        txtBio.Text = "";
        getAllActorsProducers();

    }
}