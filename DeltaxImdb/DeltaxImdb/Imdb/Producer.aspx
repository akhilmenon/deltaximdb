﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Producer.aspx.cs" Inherits="Producer" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script src="jquery-3.1.0.js"></script>
    <script src="jquery.tmpl.js"></script>
    <script id="data" type="text/x-jquery-tmpl">
        <tr>
            <td>${producer_name}</td>
            <td>${producer_sex}</td>
            <td>${producer_dob}</td>
            <td>${producer_bio}</td>
            <td>
                <a id="${producer_id_pk}del" href="" >Delete</a> | <a id="${producer_id_pk}edit" href="">Edit<//a>
            </td>
        </tr>
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            ajax("", "", "");

            $(document).on("click", "[id$=del]", function (e) {
                e.preventDefault();
                if (confirm("Are you Sure? you want to delete")) {
                    ajax(parseInt($(this).attr("id")), "del", "delProducer");
                }
            });

            $(document).on("click", "[id$=edit]", function (e) {
                e.preventDefault();
                 ajax(parseInt($(this).attr("id")), "edit", "editProducer");
            });
        });

        function ajax(data, method, func) {
            func = func == "" ? "getAllProducers" : func;
            data = JSON.stringify({ data: data });
            $.ajax({
                type: "post",
                contentType: "application/json; charset=utf-8",
                url: "Producer.aspx/"+func,
                data: data,
                dataType: "json",
                success: function (response) {
                    if (method == "edit") {
                        str = $.parseJSON(response.d)
                        $("#hdnproduceridpk").val(str[0].producer_id_pk);
                        $("#txtProducerName").val(str[0].producer_name);
                        if (str[0].producer_sex == "Male") {
                            $("#rdbmale").attr("checked","checked");
                        }
                        else if (str[0].producer_sex == "Female") {
                            $("#rdbfemale").attr("checked", "checked");
                        }
                        $("#caldob").val(str[0].producer_dob);
                        $("#caldob").attr("SelectedDate", str[0].producer_dob);
                        $("#txtBio").val(str[0].producer_bio);
                    }
                    else {
                        filldata($.parseJSON(response.d));
                    }
                },
                failure: function (response) {
                    alert(response.d);
                },
                error: function (response) {
                    alert(response);
                }
            });
        }

        function filldata(str){
            $("#tbl > tbody").empty();
            $("#data").tmpl(str).appendTo("#tbl");
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <div>
            <asp:TextBox ID="txtProducerName" runat="server" placeholder="Enter Producer Name"></asp:TextBox>
        </div>
        <div>
            <asp:RadioButton ID="rdbmale" Text="Male" GroupName="gender" runat="server" />
            <asp:RadioButton ID="rdbfemale" Text="Female" GroupName="gender" runat="server" />
        </div>
        <div>
            <asp:Calendar ID="caldob" runat="server" placeholder="Enter Dob"></asp:Calendar>
        </div>
        <div>
            <asp:TextBox ID="txtBio" TextMode="MultiLine" runat="server" placeholder="Enter Bio"></asp:TextBox>
        </div>
        <div>
            <asp:Button ID="btnsubmit" Text="Submit" runat="server" OnClick="btnsubmit_Click" />
            <asp:HiddenField ID="hdnproduceridpk" Value="" runat="server" />
        </div>
        <div>
            <table id="tbl" style="text-align:center; border:1px solid black" align="center">
            <thead  style="background-color:cyan">
                <tr>
                    <td>Producer Name</td>
                    <td>Gender</td>
                    <td>Date_Of_Birth</td>
                    <td>Bio</td>
                    <td>Action</td>
                </tr>
            </thead>
            <tbody>

            </tbody>
        </table>
        </div>
    </div>
    </form>
</body>
</html>
