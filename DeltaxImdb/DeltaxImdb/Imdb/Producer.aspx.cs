﻿using BusinessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Producer : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    [WebMethod]
    public static String getAllProducers()
    {
        tblProducer objProducer = new tblProducer();
        return objProducer.getAllProducer();
    }

    [WebMethod]
    public static string delProducer(int data)
    {
        tblProducer objProducer = new tblProducer();
        objProducer.LoadByPrimaryKey(data);
        objProducer.MarkAsDeleted();
        objProducer.Save();

        return getAllProducers();
    }

    [WebMethod]
    public static string editProducer(int data)
    {
        tblProducer objProducer = new tblProducer();
        return objProducer.getProducerDetail(data);
    }
    protected void btnsubmit_Click(object sender, EventArgs e)
    {
        tblProducer objProducer = new tblProducer();

        if (hdnproduceridpk.Value == "")
        {
            objProducer.AddNew();
        }
        else
        {
            objProducer.LoadByPrimaryKey(Convert.ToInt32(hdnproduceridpk.Value));
        }

        objProducer.Producer_name = txtProducerName.Text;
        if (rdbmale.Checked)
            objProducer.Producer_sex = rdbmale.Text;
        else if (rdbfemale.Checked)
            objProducer.Producer_sex = rdbfemale.Text;
        objProducer.Producer_bio = txtBio.Text;
        objProducer.Producer_dob = caldob.SelectedDate.ToString();

        objProducer.Save();

        txtProducerName.Text = "";
        txtBio.Text = "";
    }
}