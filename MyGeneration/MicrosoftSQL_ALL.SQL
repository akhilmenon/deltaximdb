
USE [deltaximdb]
GO

/****** Object:  StoredProcedure [proc_tblActorLoadByPrimaryKey]    Script Date: 30-May-18 19:10:43 ******/
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[proc_tblActorLoadByPrimaryKey]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
    DROP PROCEDURE [proc_tblActorLoadByPrimaryKey];
GO

CREATE PROCEDURE [proc_tblActorLoadByPrimaryKey]
(
	@actor_id_pk int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err int

	SELECT
		[actor_id_pk],
		[actor_name],
		[actor_sex],
		[actor_dob],
		[actor_bio]
	FROM [tblActor]
	WHERE
		([actor_id_pk] = @actor_id_pk)

	SET @Err = @@Error

	RETURN @Err
END
GO


-- Display the status of Proc creation
IF (@@Error = 0) PRINT 'Procedure Creation: proc_tblActorLoadByPrimaryKey Succeeded'
ELSE PRINT 'Procedure Creation: proc_tblActorLoadByPrimaryKey Error on Creation'
GO

/****** Object:  StoredProcedure [proc_tblActorLoadAll]    Script Date: 30-May-18 19:10:43 ******/
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[proc_tblActorLoadAll]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
    DROP PROCEDURE [proc_tblActorLoadAll];
GO

CREATE PROCEDURE [proc_tblActorLoadAll]
AS
BEGIN

	SET NOCOUNT ON
	DECLARE @Err int

	SELECT
		[actor_id_pk],
		[actor_name],
		[actor_sex],
		[actor_dob],
		[actor_bio]
	FROM [tblActor]

	SET @Err = @@Error

	RETURN @Err
END
GO


-- Display the status of Proc creation
IF (@@Error = 0) PRINT 'Procedure Creation: proc_tblActorLoadAll Succeeded'
ELSE PRINT 'Procedure Creation: proc_tblActorLoadAll Error on Creation'
GO

/****** Object:  StoredProcedure [proc_tblActorUpdate]    Script Date: 30-May-18 19:10:43 ******/
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[proc_tblActorUpdate]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
    DROP PROCEDURE [proc_tblActorUpdate];
GO

CREATE PROCEDURE [proc_tblActorUpdate]
(
	@actor_id_pk int,
	@actor_name nvarchar(50) = NULL,
	@actor_sex nvarchar(50) = NULL,
	@actor_dob nvarchar(150) = NULL,
	@actor_bio nvarchar(50) = NULL
)
AS
BEGIN

	SET NOCOUNT OFF
	DECLARE @Err int

	UPDATE [tblActor]
	SET
		[actor_name] = @actor_name,
		[actor_sex] = @actor_sex,
		[actor_dob] = @actor_dob,
		[actor_bio] = @actor_bio
	WHERE
		[actor_id_pk] = @actor_id_pk


	SET @Err = @@Error


	RETURN @Err
END
GO


-- Display the status of Proc creation
IF (@@Error = 0) PRINT 'Procedure Creation: proc_tblActorUpdate Succeeded'
ELSE PRINT 'Procedure Creation: proc_tblActorUpdate Error on Creation'
GO




/****** Object:  StoredProcedure [proc_tblActorInsert]    Script Date: 30-May-18 19:10:43 ******/
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[proc_tblActorInsert]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
    DROP PROCEDURE [proc_tblActorInsert];
GO

CREATE PROCEDURE [proc_tblActorInsert]
(
	@actor_id_pk int = NULL output,
	@actor_name nvarchar(50) = NULL,
	@actor_sex nvarchar(50) = NULL,
	@actor_dob nvarchar(150) = NULL,
	@actor_bio nvarchar(50) = NULL
)
AS
BEGIN

	SET NOCOUNT OFF
	DECLARE @Err int

	INSERT
	INTO [tblActor]
	(
		[actor_name],
		[actor_sex],
		[actor_dob],
		[actor_bio]
	)
	VALUES
	(
		@actor_name,
		@actor_sex,
		@actor_dob,
		@actor_bio
	)

	SET @Err = @@Error

	SELECT @actor_id_pk = SCOPE_IDENTITY()

	RETURN @Err
END
GO


-- Display the status of Proc creation
IF (@@Error = 0) PRINT 'Procedure Creation: proc_tblActorInsert Succeeded'
ELSE PRINT 'Procedure Creation: proc_tblActorInsert Error on Creation'
GO

/****** Object:  StoredProcedure [proc_tblActorDelete]    Script Date: 30-May-18 19:10:43 ******/
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[proc_tblActorDelete]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
    DROP PROCEDURE [proc_tblActorDelete];
GO

CREATE PROCEDURE [proc_tblActorDelete]
(
	@actor_id_pk int
)
AS
BEGIN

	SET NOCOUNT OFF
	DECLARE @Err int

	DELETE
	FROM [tblActor]
	WHERE
		[actor_id_pk] = @actor_id_pk
	SET @Err = @@Error

	RETURN @Err
END
GO


-- Display the status of Proc creation
IF (@@Error = 0) PRINT 'Procedure Creation: proc_tblActorDelete Succeeded'
ELSE PRINT 'Procedure Creation: proc_tblActorDelete Error on Creation'
GO

/****** Object:  StoredProcedure [proc_tblActorxMovieLoadByPrimaryKey]    Script Date: 30-May-18 19:10:43 ******/
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[proc_tblActorxMovieLoadByPrimaryKey]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
    DROP PROCEDURE [proc_tblActorxMovieLoadByPrimaryKey];
GO

CREATE PROCEDURE [proc_tblActorxMovieLoadByPrimaryKey]
(
	@actor_movie_id_pk int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err int

	SELECT
		[actor_movie_id_pk],
		[actor_id_fk],
		[movie_id_fk]
	FROM [tblActorxMovie]
	WHERE
		([actor_movie_id_pk] = @actor_movie_id_pk)

	SET @Err = @@Error

	RETURN @Err
END
GO


-- Display the status of Proc creation
IF (@@Error = 0) PRINT 'Procedure Creation: proc_tblActorxMovieLoadByPrimaryKey Succeeded'
ELSE PRINT 'Procedure Creation: proc_tblActorxMovieLoadByPrimaryKey Error on Creation'
GO

/****** Object:  StoredProcedure [proc_tblActorxMovieLoadAll]    Script Date: 30-May-18 19:10:43 ******/
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[proc_tblActorxMovieLoadAll]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
    DROP PROCEDURE [proc_tblActorxMovieLoadAll];
GO

CREATE PROCEDURE [proc_tblActorxMovieLoadAll]
AS
BEGIN

	SET NOCOUNT ON
	DECLARE @Err int

	SELECT
		[actor_movie_id_pk],
		[actor_id_fk],
		[movie_id_fk]
	FROM [tblActorxMovie]

	SET @Err = @@Error

	RETURN @Err
END
GO


-- Display the status of Proc creation
IF (@@Error = 0) PRINT 'Procedure Creation: proc_tblActorxMovieLoadAll Succeeded'
ELSE PRINT 'Procedure Creation: proc_tblActorxMovieLoadAll Error on Creation'
GO

/****** Object:  StoredProcedure [proc_tblActorxMovieUpdate]    Script Date: 30-May-18 19:10:43 ******/
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[proc_tblActorxMovieUpdate]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
    DROP PROCEDURE [proc_tblActorxMovieUpdate];
GO

CREATE PROCEDURE [proc_tblActorxMovieUpdate]
(
	@actor_movie_id_pk int,
	@actor_id_fk int = NULL,
	@movie_id_fk int = NULL
)
AS
BEGIN

	SET NOCOUNT OFF
	DECLARE @Err int

	UPDATE [tblActorxMovie]
	SET
		[actor_id_fk] = @actor_id_fk,
		[movie_id_fk] = @movie_id_fk
	WHERE
		[actor_movie_id_pk] = @actor_movie_id_pk


	SET @Err = @@Error


	RETURN @Err
END
GO


-- Display the status of Proc creation
IF (@@Error = 0) PRINT 'Procedure Creation: proc_tblActorxMovieUpdate Succeeded'
ELSE PRINT 'Procedure Creation: proc_tblActorxMovieUpdate Error on Creation'
GO




/****** Object:  StoredProcedure [proc_tblActorxMovieInsert]    Script Date: 30-May-18 19:10:43 ******/
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[proc_tblActorxMovieInsert]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
    DROP PROCEDURE [proc_tblActorxMovieInsert];
GO

CREATE PROCEDURE [proc_tblActorxMovieInsert]
(
	@actor_movie_id_pk int = NULL output,
	@actor_id_fk int = NULL,
	@movie_id_fk int = NULL
)
AS
BEGIN

	SET NOCOUNT OFF
	DECLARE @Err int

	INSERT
	INTO [tblActorxMovie]
	(
		[actor_id_fk],
		[movie_id_fk]
	)
	VALUES
	(
		@actor_id_fk,
		@movie_id_fk
	)

	SET @Err = @@Error

	SELECT @actor_movie_id_pk = SCOPE_IDENTITY()

	RETURN @Err
END
GO


-- Display the status of Proc creation
IF (@@Error = 0) PRINT 'Procedure Creation: proc_tblActorxMovieInsert Succeeded'
ELSE PRINT 'Procedure Creation: proc_tblActorxMovieInsert Error on Creation'
GO

/****** Object:  StoredProcedure [proc_tblActorxMovieDelete]    Script Date: 30-May-18 19:10:43 ******/
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[proc_tblActorxMovieDelete]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
    DROP PROCEDURE [proc_tblActorxMovieDelete];
GO

CREATE PROCEDURE [proc_tblActorxMovieDelete]
(
	@actor_movie_id_pk int
)
AS
BEGIN

	SET NOCOUNT OFF
	DECLARE @Err int

	DELETE
	FROM [tblActorxMovie]
	WHERE
		[actor_movie_id_pk] = @actor_movie_id_pk
	SET @Err = @@Error

	RETURN @Err
END
GO


-- Display the status of Proc creation
IF (@@Error = 0) PRINT 'Procedure Creation: proc_tblActorxMovieDelete Succeeded'
ELSE PRINT 'Procedure Creation: proc_tblActorxMovieDelete Error on Creation'
GO

/****** Object:  StoredProcedure [proc_tblMovieLoadByPrimaryKey]    Script Date: 30-May-18 19:10:43 ******/
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[proc_tblMovieLoadByPrimaryKey]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
    DROP PROCEDURE [proc_tblMovieLoadByPrimaryKey];
GO

CREATE PROCEDURE [proc_tblMovieLoadByPrimaryKey]
(
	@movie_id_pk int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err int

	SELECT
		[movie_id_pk],
		[movie_name],
		[movie_release_year],
		[movie_plot],
		[movie_poster_image]
	FROM [tblMovie]
	WHERE
		([movie_id_pk] = @movie_id_pk)

	SET @Err = @@Error

	RETURN @Err
END
GO


-- Display the status of Proc creation
IF (@@Error = 0) PRINT 'Procedure Creation: proc_tblMovieLoadByPrimaryKey Succeeded'
ELSE PRINT 'Procedure Creation: proc_tblMovieLoadByPrimaryKey Error on Creation'
GO

/****** Object:  StoredProcedure [proc_tblMovieLoadAll]    Script Date: 30-May-18 19:10:43 ******/
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[proc_tblMovieLoadAll]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
    DROP PROCEDURE [proc_tblMovieLoadAll];
GO

CREATE PROCEDURE [proc_tblMovieLoadAll]
AS
BEGIN

	SET NOCOUNT ON
	DECLARE @Err int

	SELECT
		[movie_id_pk],
		[movie_name],
		[movie_release_year],
		[movie_plot],
		[movie_poster_image]
	FROM [tblMovie]

	SET @Err = @@Error

	RETURN @Err
END
GO


-- Display the status of Proc creation
IF (@@Error = 0) PRINT 'Procedure Creation: proc_tblMovieLoadAll Succeeded'
ELSE PRINT 'Procedure Creation: proc_tblMovieLoadAll Error on Creation'
GO

/****** Object:  StoredProcedure [proc_tblMovieUpdate]    Script Date: 30-May-18 19:10:43 ******/
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[proc_tblMovieUpdate]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
    DROP PROCEDURE [proc_tblMovieUpdate];
GO

CREATE PROCEDURE [proc_tblMovieUpdate]
(
	@movie_id_pk int,
	@movie_name nvarchar(50) = NULL,
	@movie_release_year nvarchar(50) = NULL,
	@movie_plot nvarchar(50) = NULL,
	@movie_poster_image nvarchar(150) = NULL
)
AS
BEGIN

	SET NOCOUNT OFF
	DECLARE @Err int

	UPDATE [tblMovie]
	SET
		[movie_name] = @movie_name,
		[movie_release_year] = @movie_release_year,
		[movie_plot] = @movie_plot,
		[movie_poster_image] = @movie_poster_image
	WHERE
		[movie_id_pk] = @movie_id_pk


	SET @Err = @@Error


	RETURN @Err
END
GO


-- Display the status of Proc creation
IF (@@Error = 0) PRINT 'Procedure Creation: proc_tblMovieUpdate Succeeded'
ELSE PRINT 'Procedure Creation: proc_tblMovieUpdate Error on Creation'
GO




/****** Object:  StoredProcedure [proc_tblMovieInsert]    Script Date: 30-May-18 19:10:43 ******/
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[proc_tblMovieInsert]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
    DROP PROCEDURE [proc_tblMovieInsert];
GO

CREATE PROCEDURE [proc_tblMovieInsert]
(
	@movie_id_pk int = NULL output,
	@movie_name nvarchar(50) = NULL,
	@movie_release_year nvarchar(50) = NULL,
	@movie_plot nvarchar(50) = NULL,
	@movie_poster_image nvarchar(150) = NULL
)
AS
BEGIN

	SET NOCOUNT OFF
	DECLARE @Err int

	INSERT
	INTO [tblMovie]
	(
		[movie_name],
		[movie_release_year],
		[movie_plot],
		[movie_poster_image]
	)
	VALUES
	(
		@movie_name,
		@movie_release_year,
		@movie_plot,
		@movie_poster_image
	)

	SET @Err = @@Error

	SELECT @movie_id_pk = SCOPE_IDENTITY()

	RETURN @Err
END
GO


-- Display the status of Proc creation
IF (@@Error = 0) PRINT 'Procedure Creation: proc_tblMovieInsert Succeeded'
ELSE PRINT 'Procedure Creation: proc_tblMovieInsert Error on Creation'
GO

/****** Object:  StoredProcedure [proc_tblMovieDelete]    Script Date: 30-May-18 19:10:43 ******/
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[proc_tblMovieDelete]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
    DROP PROCEDURE [proc_tblMovieDelete];
GO

CREATE PROCEDURE [proc_tblMovieDelete]
(
	@movie_id_pk int
)
AS
BEGIN

	SET NOCOUNT OFF
	DECLARE @Err int

	DELETE
	FROM [tblMovie]
	WHERE
		[movie_id_pk] = @movie_id_pk
	SET @Err = @@Error

	RETURN @Err
END
GO


-- Display the status of Proc creation
IF (@@Error = 0) PRINT 'Procedure Creation: proc_tblMovieDelete Succeeded'
ELSE PRINT 'Procedure Creation: proc_tblMovieDelete Error on Creation'
GO

/****** Object:  StoredProcedure [proc_tblProducerLoadByPrimaryKey]    Script Date: 30-May-18 19:10:43 ******/
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[proc_tblProducerLoadByPrimaryKey]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
    DROP PROCEDURE [proc_tblProducerLoadByPrimaryKey];
GO

CREATE PROCEDURE [proc_tblProducerLoadByPrimaryKey]
(
	@producer_id_pk int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err int

	SELECT
		[producer_id_pk],
		[producer_name],
		[producer_sex],
		[producer_dob],
		[producer_bio]
	FROM [tblProducer]
	WHERE
		([producer_id_pk] = @producer_id_pk)

	SET @Err = @@Error

	RETURN @Err
END
GO


-- Display the status of Proc creation
IF (@@Error = 0) PRINT 'Procedure Creation: proc_tblProducerLoadByPrimaryKey Succeeded'
ELSE PRINT 'Procedure Creation: proc_tblProducerLoadByPrimaryKey Error on Creation'
GO

/****** Object:  StoredProcedure [proc_tblProducerLoadAll]    Script Date: 30-May-18 19:10:43 ******/
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[proc_tblProducerLoadAll]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
    DROP PROCEDURE [proc_tblProducerLoadAll];
GO

CREATE PROCEDURE [proc_tblProducerLoadAll]
AS
BEGIN

	SET NOCOUNT ON
	DECLARE @Err int

	SELECT
		[producer_id_pk],
		[producer_name],
		[producer_sex],
		[producer_dob],
		[producer_bio]
	FROM [tblProducer]

	SET @Err = @@Error

	RETURN @Err
END
GO


-- Display the status of Proc creation
IF (@@Error = 0) PRINT 'Procedure Creation: proc_tblProducerLoadAll Succeeded'
ELSE PRINT 'Procedure Creation: proc_tblProducerLoadAll Error on Creation'
GO

/****** Object:  StoredProcedure [proc_tblProducerUpdate]    Script Date: 30-May-18 19:10:43 ******/
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[proc_tblProducerUpdate]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
    DROP PROCEDURE [proc_tblProducerUpdate];
GO

CREATE PROCEDURE [proc_tblProducerUpdate]
(
	@producer_id_pk int,
	@producer_name nvarchar(50) = NULL,
	@producer_sex nvarchar(50) = NULL,
	@producer_dob nvarchar(50) = NULL,
	@producer_bio nchar(10) = NULL
)
AS
BEGIN

	SET NOCOUNT OFF
	DECLARE @Err int

	UPDATE [tblProducer]
	SET
		[producer_name] = @producer_name,
		[producer_sex] = @producer_sex,
		[producer_dob] = @producer_dob,
		[producer_bio] = @producer_bio
	WHERE
		[producer_id_pk] = @producer_id_pk


	SET @Err = @@Error


	RETURN @Err
END
GO


-- Display the status of Proc creation
IF (@@Error = 0) PRINT 'Procedure Creation: proc_tblProducerUpdate Succeeded'
ELSE PRINT 'Procedure Creation: proc_tblProducerUpdate Error on Creation'
GO




/****** Object:  StoredProcedure [proc_tblProducerInsert]    Script Date: 30-May-18 19:10:43 ******/
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[proc_tblProducerInsert]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
    DROP PROCEDURE [proc_tblProducerInsert];
GO

CREATE PROCEDURE [proc_tblProducerInsert]
(
	@producer_id_pk int = NULL output,
	@producer_name nvarchar(50) = NULL,
	@producer_sex nvarchar(50) = NULL,
	@producer_dob nvarchar(50) = NULL,
	@producer_bio nchar(10) = NULL
)
AS
BEGIN

	SET NOCOUNT OFF
	DECLARE @Err int

	INSERT
	INTO [tblProducer]
	(
		[producer_name],
		[producer_sex],
		[producer_dob],
		[producer_bio]
	)
	VALUES
	(
		@producer_name,
		@producer_sex,
		@producer_dob,
		@producer_bio
	)

	SET @Err = @@Error

	SELECT @producer_id_pk = SCOPE_IDENTITY()

	RETURN @Err
END
GO


-- Display the status of Proc creation
IF (@@Error = 0) PRINT 'Procedure Creation: proc_tblProducerInsert Succeeded'
ELSE PRINT 'Procedure Creation: proc_tblProducerInsert Error on Creation'
GO

/****** Object:  StoredProcedure [proc_tblProducerDelete]    Script Date: 30-May-18 19:10:43 ******/
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[proc_tblProducerDelete]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
    DROP PROCEDURE [proc_tblProducerDelete];
GO

CREATE PROCEDURE [proc_tblProducerDelete]
(
	@producer_id_pk int
)
AS
BEGIN

	SET NOCOUNT OFF
	DECLARE @Err int

	DELETE
	FROM [tblProducer]
	WHERE
		[producer_id_pk] = @producer_id_pk
	SET @Err = @@Error

	RETURN @Err
END
GO


-- Display the status of Proc creation
IF (@@Error = 0) PRINT 'Procedure Creation: proc_tblProducerDelete Succeeded'
ELSE PRINT 'Procedure Creation: proc_tblProducerDelete Error on Creation'
GO

/****** Object:  StoredProcedure [proc_tblProducerxMovieLoadByPrimaryKey]    Script Date: 30-May-18 19:10:43 ******/
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[proc_tblProducerxMovieLoadByPrimaryKey]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
    DROP PROCEDURE [proc_tblProducerxMovieLoadByPrimaryKey];
GO

CREATE PROCEDURE [proc_tblProducerxMovieLoadByPrimaryKey]
(
	@producer_movie_id_pk int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err int

	SELECT
		[producer_movie_id_pk],
		[producer_id_fk],
		[movie_id_fk]
	FROM [tblProducerxMovie]
	WHERE
		([producer_movie_id_pk] = @producer_movie_id_pk)

	SET @Err = @@Error

	RETURN @Err
END
GO


-- Display the status of Proc creation
IF (@@Error = 0) PRINT 'Procedure Creation: proc_tblProducerxMovieLoadByPrimaryKey Succeeded'
ELSE PRINT 'Procedure Creation: proc_tblProducerxMovieLoadByPrimaryKey Error on Creation'
GO

/****** Object:  StoredProcedure [proc_tblProducerxMovieLoadAll]    Script Date: 30-May-18 19:10:43 ******/
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[proc_tblProducerxMovieLoadAll]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
    DROP PROCEDURE [proc_tblProducerxMovieLoadAll];
GO

CREATE PROCEDURE [proc_tblProducerxMovieLoadAll]
AS
BEGIN

	SET NOCOUNT ON
	DECLARE @Err int

	SELECT
		[producer_movie_id_pk],
		[producer_id_fk],
		[movie_id_fk]
	FROM [tblProducerxMovie]

	SET @Err = @@Error

	RETURN @Err
END
GO


-- Display the status of Proc creation
IF (@@Error = 0) PRINT 'Procedure Creation: proc_tblProducerxMovieLoadAll Succeeded'
ELSE PRINT 'Procedure Creation: proc_tblProducerxMovieLoadAll Error on Creation'
GO

/****** Object:  StoredProcedure [proc_tblProducerxMovieUpdate]    Script Date: 30-May-18 19:10:43 ******/
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[proc_tblProducerxMovieUpdate]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
    DROP PROCEDURE [proc_tblProducerxMovieUpdate];
GO

CREATE PROCEDURE [proc_tblProducerxMovieUpdate]
(
	@producer_movie_id_pk int,
	@producer_id_fk int = NULL,
	@movie_id_fk int = NULL
)
AS
BEGIN

	SET NOCOUNT OFF
	DECLARE @Err int

	UPDATE [tblProducerxMovie]
	SET
		[producer_id_fk] = @producer_id_fk,
		[movie_id_fk] = @movie_id_fk
	WHERE
		[producer_movie_id_pk] = @producer_movie_id_pk


	SET @Err = @@Error


	RETURN @Err
END
GO


-- Display the status of Proc creation
IF (@@Error = 0) PRINT 'Procedure Creation: proc_tblProducerxMovieUpdate Succeeded'
ELSE PRINT 'Procedure Creation: proc_tblProducerxMovieUpdate Error on Creation'
GO




/****** Object:  StoredProcedure [proc_tblProducerxMovieInsert]    Script Date: 30-May-18 19:10:43 ******/
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[proc_tblProducerxMovieInsert]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
    DROP PROCEDURE [proc_tblProducerxMovieInsert];
GO

CREATE PROCEDURE [proc_tblProducerxMovieInsert]
(
	@producer_movie_id_pk int = NULL output,
	@producer_id_fk int = NULL,
	@movie_id_fk int = NULL
)
AS
BEGIN

	SET NOCOUNT OFF
	DECLARE @Err int

	INSERT
	INTO [tblProducerxMovie]
	(
		[producer_id_fk],
		[movie_id_fk]
	)
	VALUES
	(
		@producer_id_fk,
		@movie_id_fk
	)

	SET @Err = @@Error

	SELECT @producer_movie_id_pk = SCOPE_IDENTITY()

	RETURN @Err
END
GO


-- Display the status of Proc creation
IF (@@Error = 0) PRINT 'Procedure Creation: proc_tblProducerxMovieInsert Succeeded'
ELSE PRINT 'Procedure Creation: proc_tblProducerxMovieInsert Error on Creation'
GO

/****** Object:  StoredProcedure [proc_tblProducerxMovieDelete]    Script Date: 30-May-18 19:10:43 ******/
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[proc_tblProducerxMovieDelete]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
    DROP PROCEDURE [proc_tblProducerxMovieDelete];
GO

CREATE PROCEDURE [proc_tblProducerxMovieDelete]
(
	@producer_movie_id_pk int
)
AS
BEGIN

	SET NOCOUNT OFF
	DECLARE @Err int

	DELETE
	FROM [tblProducerxMovie]
	WHERE
		[producer_movie_id_pk] = @producer_movie_id_pk
	SET @Err = @@Error

	RETURN @Err
END
GO


-- Display the status of Proc creation
IF (@@Error = 0) PRINT 'Procedure Creation: proc_tblProducerxMovieDelete Succeeded'
ELSE PRINT 'Procedure Creation: proc_tblProducerxMovieDelete Error on Creation'
GO
