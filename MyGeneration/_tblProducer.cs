
/*
'===============================================================================
'  Generated From - CSharp_dOOdads_BusinessEntity.vbgen
' 
'  ** IMPORTANT  ** 
'  How to Generate your stored procedures:
' 
'  SQL        = SQL_StoredProcs.vbgen
'  ACCESS     = Access_StoredProcs.vbgen
'  ORACLE     = Oracle_StoredProcs.vbgen
'  FIREBIRD   = FirebirdStoredProcs.vbgen
'  POSTGRESQL = PostgreSQL_StoredProcs.vbgen
'
'  The supporting base class SqlClientEntity is in the Architecture directory in "dOOdads".
'  
'  This object is 'abstract' which means you need to inherit from it to be able
'  to instantiate it.  This is very easilly done. You can override properties and
'  methods in your derived class, this allows you to regenerate this class at any
'  time and not worry about overwriting custom code. 
'
'  NEVER EDIT THIS FILE.
'
'  public class YourObject :  _YourObject
'  {
'
'  }
'
'===============================================================================
*/

// Generated by MyGeneration Version # (1.3.0.3)

using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.Collections.Specialized;

using MyGeneration.dOOdads;

namespace BusinessLayer
{
	public abstract class _tblProducer : SqlClientEntity
	{
		public _tblProducer()
		{
			this.QuerySource = "tblProducer";
			this.MappingName = "tblProducer";

		}	

		//=================================================================
		//  public Overrides void AddNew()
		//=================================================================
		//
		//=================================================================
		public override void AddNew()
		{
			base.AddNew();
			
		}
		
		
		public override void FlushData()
		{
			this._whereClause = null;
			this._aggregateClause = null;
			base.FlushData();
		}
		
		//=================================================================
		//  	public Function LoadAll() As Boolean
		//=================================================================
		//  Loads all of the records in the database, and sets the currentRow to the first row
		//=================================================================
		public bool LoadAll() 
		{
			ListDictionary parameters = null;
			
			return base.LoadFromSql("[" + this.SchemaStoredProcedure + "proc_tblProducerLoadAll]", parameters);
		}
	
		//=================================================================
		// public Overridable Function LoadByPrimaryKey()  As Boolean
		//=================================================================
		//  Loads a single row of via the primary key
		//=================================================================
		public virtual bool LoadByPrimaryKey(int Producer_id_pk)
		{
			ListDictionary parameters = new ListDictionary();
			parameters.Add(Parameters.Producer_id_pk, Producer_id_pk);

		
			return base.LoadFromSql("[" + this.SchemaStoredProcedure + "proc_tblProducerLoadByPrimaryKey]", parameters);
		}
		
		#region Parameters
		protected class Parameters
		{
			
			public static SqlParameter Producer_id_pk
			{
				get
				{
					return new SqlParameter("@Producer_id_pk", SqlDbType.Int, 0);
				}
			}
			
			public static SqlParameter Producer_name
			{
				get
				{
					return new SqlParameter("@Producer_name", SqlDbType.NVarChar, 50);
				}
			}
			
			public static SqlParameter Producer_sex
			{
				get
				{
					return new SqlParameter("@Producer_sex", SqlDbType.NVarChar, 50);
				}
			}
			
			public static SqlParameter Producer_dob
			{
				get
				{
					return new SqlParameter("@Producer_dob", SqlDbType.NVarChar, 50);
				}
			}
			
			public static SqlParameter Producer_bio
			{
				get
				{
					return new SqlParameter("@Producer_bio", SqlDbType.NChar, 10);
				}
			}
			
		}
		#endregion		
	
		#region ColumnNames
		public class ColumnNames
		{  
            public const string Producer_id_pk = "producer_id_pk";
            public const string Producer_name = "producer_name";
            public const string Producer_sex = "producer_sex";
            public const string Producer_dob = "producer_dob";
            public const string Producer_bio = "producer_bio";

			static public string ToPropertyName(string columnName)
			{
				if(ht == null)
				{
					ht = new Hashtable();
					
					ht[Producer_id_pk] = _tblProducer.PropertyNames.Producer_id_pk;
					ht[Producer_name] = _tblProducer.PropertyNames.Producer_name;
					ht[Producer_sex] = _tblProducer.PropertyNames.Producer_sex;
					ht[Producer_dob] = _tblProducer.PropertyNames.Producer_dob;
					ht[Producer_bio] = _tblProducer.PropertyNames.Producer_bio;

				}
				return (string)ht[columnName];
			}

			static private Hashtable ht = null;			 
		}
		#endregion
		
		#region PropertyNames
		public class PropertyNames
		{  
            public const string Producer_id_pk = "Producer_id_pk";
            public const string Producer_name = "Producer_name";
            public const string Producer_sex = "Producer_sex";
            public const string Producer_dob = "Producer_dob";
            public const string Producer_bio = "Producer_bio";

			static public string ToColumnName(string propertyName)
			{
				if(ht == null)
				{
					ht = new Hashtable();
					
					ht[Producer_id_pk] = _tblProducer.ColumnNames.Producer_id_pk;
					ht[Producer_name] = _tblProducer.ColumnNames.Producer_name;
					ht[Producer_sex] = _tblProducer.ColumnNames.Producer_sex;
					ht[Producer_dob] = _tblProducer.ColumnNames.Producer_dob;
					ht[Producer_bio] = _tblProducer.ColumnNames.Producer_bio;

				}
				return (string)ht[propertyName];
			}

			static private Hashtable ht = null;			 
		}			 
		#endregion	

		#region StringPropertyNames
		public class StringPropertyNames
		{  
            public const string Producer_id_pk = "s_Producer_id_pk";
            public const string Producer_name = "s_Producer_name";
            public const string Producer_sex = "s_Producer_sex";
            public const string Producer_dob = "s_Producer_dob";
            public const string Producer_bio = "s_Producer_bio";

		}
		#endregion		
		
		#region Properties
	
		public virtual int Producer_id_pk
	    {
			get
	        {
				return base.Getint(ColumnNames.Producer_id_pk);
			}
			set
	        {
				base.Setint(ColumnNames.Producer_id_pk, value);
			}
		}

		public virtual string Producer_name
	    {
			get
	        {
				return base.Getstring(ColumnNames.Producer_name);
			}
			set
	        {
				base.Setstring(ColumnNames.Producer_name, value);
			}
		}

		public virtual string Producer_sex
	    {
			get
	        {
				return base.Getstring(ColumnNames.Producer_sex);
			}
			set
	        {
				base.Setstring(ColumnNames.Producer_sex, value);
			}
		}

		public virtual string Producer_dob
	    {
			get
	        {
				return base.Getstring(ColumnNames.Producer_dob);
			}
			set
	        {
				base.Setstring(ColumnNames.Producer_dob, value);
			}
		}

		public virtual string Producer_bio
	    {
			get
	        {
				return base.Getstring(ColumnNames.Producer_bio);
			}
			set
	        {
				base.Setstring(ColumnNames.Producer_bio, value);
			}
		}


		#endregion
		
		#region String Properties
	
		public virtual string s_Producer_id_pk
	    {
			get
	        {
				return this.IsColumnNull(ColumnNames.Producer_id_pk) ? string.Empty : base.GetintAsString(ColumnNames.Producer_id_pk);
			}
			set
	        {
				if(string.Empty == value)
					this.SetColumnNull(ColumnNames.Producer_id_pk);
				else
					this.Producer_id_pk = base.SetintAsString(ColumnNames.Producer_id_pk, value);
			}
		}

		public virtual string s_Producer_name
	    {
			get
	        {
				return this.IsColumnNull(ColumnNames.Producer_name) ? string.Empty : base.GetstringAsString(ColumnNames.Producer_name);
			}
			set
	        {
				if(string.Empty == value)
					this.SetColumnNull(ColumnNames.Producer_name);
				else
					this.Producer_name = base.SetstringAsString(ColumnNames.Producer_name, value);
			}
		}

		public virtual string s_Producer_sex
	    {
			get
	        {
				return this.IsColumnNull(ColumnNames.Producer_sex) ? string.Empty : base.GetstringAsString(ColumnNames.Producer_sex);
			}
			set
	        {
				if(string.Empty == value)
					this.SetColumnNull(ColumnNames.Producer_sex);
				else
					this.Producer_sex = base.SetstringAsString(ColumnNames.Producer_sex, value);
			}
		}

		public virtual string s_Producer_dob
	    {
			get
	        {
				return this.IsColumnNull(ColumnNames.Producer_dob) ? string.Empty : base.GetstringAsString(ColumnNames.Producer_dob);
			}
			set
	        {
				if(string.Empty == value)
					this.SetColumnNull(ColumnNames.Producer_dob);
				else
					this.Producer_dob = base.SetstringAsString(ColumnNames.Producer_dob, value);
			}
		}

		public virtual string s_Producer_bio
	    {
			get
	        {
				return this.IsColumnNull(ColumnNames.Producer_bio) ? string.Empty : base.GetstringAsString(ColumnNames.Producer_bio);
			}
			set
	        {
				if(string.Empty == value)
					this.SetColumnNull(ColumnNames.Producer_bio);
				else
					this.Producer_bio = base.SetstringAsString(ColumnNames.Producer_bio, value);
			}
		}


		#endregion		
	
		#region Where Clause
		public class WhereClause
		{
			public WhereClause(BusinessEntity entity)
			{
				this._entity = entity;
			}
			
			public TearOffWhereParameter TearOff
			{
				get
				{
					if(_tearOff == null)
					{
						_tearOff = new TearOffWhereParameter(this);
					}

					return _tearOff;
				}
			}

			#region WhereParameter TearOff's
			public class TearOffWhereParameter
			{
				public TearOffWhereParameter(WhereClause clause)
				{
					this._clause = clause;
				}
				
				
				public WhereParameter Producer_id_pk
				{
					get
					{
							WhereParameter where = new WhereParameter(ColumnNames.Producer_id_pk, Parameters.Producer_id_pk);
							this._clause._entity.Query.AddWhereParameter(where);
							return where;
					}
				}

				public WhereParameter Producer_name
				{
					get
					{
							WhereParameter where = new WhereParameter(ColumnNames.Producer_name, Parameters.Producer_name);
							this._clause._entity.Query.AddWhereParameter(where);
							return where;
					}
				}

				public WhereParameter Producer_sex
				{
					get
					{
							WhereParameter where = new WhereParameter(ColumnNames.Producer_sex, Parameters.Producer_sex);
							this._clause._entity.Query.AddWhereParameter(where);
							return where;
					}
				}

				public WhereParameter Producer_dob
				{
					get
					{
							WhereParameter where = new WhereParameter(ColumnNames.Producer_dob, Parameters.Producer_dob);
							this._clause._entity.Query.AddWhereParameter(where);
							return where;
					}
				}

				public WhereParameter Producer_bio
				{
					get
					{
							WhereParameter where = new WhereParameter(ColumnNames.Producer_bio, Parameters.Producer_bio);
							this._clause._entity.Query.AddWhereParameter(where);
							return where;
					}
				}


				private WhereClause _clause;
			}
			#endregion
		
			public WhereParameter Producer_id_pk
		    {
				get
		        {
					if(_Producer_id_pk_W == null)
	        	    {
						_Producer_id_pk_W = TearOff.Producer_id_pk;
					}
					return _Producer_id_pk_W;
				}
			}

			public WhereParameter Producer_name
		    {
				get
		        {
					if(_Producer_name_W == null)
	        	    {
						_Producer_name_W = TearOff.Producer_name;
					}
					return _Producer_name_W;
				}
			}

			public WhereParameter Producer_sex
		    {
				get
		        {
					if(_Producer_sex_W == null)
	        	    {
						_Producer_sex_W = TearOff.Producer_sex;
					}
					return _Producer_sex_W;
				}
			}

			public WhereParameter Producer_dob
		    {
				get
		        {
					if(_Producer_dob_W == null)
	        	    {
						_Producer_dob_W = TearOff.Producer_dob;
					}
					return _Producer_dob_W;
				}
			}

			public WhereParameter Producer_bio
		    {
				get
		        {
					if(_Producer_bio_W == null)
	        	    {
						_Producer_bio_W = TearOff.Producer_bio;
					}
					return _Producer_bio_W;
				}
			}

			private WhereParameter _Producer_id_pk_W = null;
			private WhereParameter _Producer_name_W = null;
			private WhereParameter _Producer_sex_W = null;
			private WhereParameter _Producer_dob_W = null;
			private WhereParameter _Producer_bio_W = null;

			public void WhereClauseReset()
			{
				_Producer_id_pk_W = null;
				_Producer_name_W = null;
				_Producer_sex_W = null;
				_Producer_dob_W = null;
				_Producer_bio_W = null;

				this._entity.Query.FlushWhereParameters();

			}
	
			private BusinessEntity _entity;
			private TearOffWhereParameter _tearOff;
			
		}
	
		public WhereClause Where
		{
			get
			{
				if(_whereClause == null)
				{
					_whereClause = new WhereClause(this);
				}
		
				return _whereClause;
			}
		}
		
		private WhereClause _whereClause = null;	
		#endregion
	
		#region Aggregate Clause
		public class AggregateClause
		{
			public AggregateClause(BusinessEntity entity)
			{
				this._entity = entity;
			}
			
			public TearOffAggregateParameter TearOff
			{
				get
				{
					if(_tearOff == null)
					{
						_tearOff = new TearOffAggregateParameter(this);
					}

					return _tearOff;
				}
			}

			#region AggregateParameter TearOff's
			public class TearOffAggregateParameter
			{
				public TearOffAggregateParameter(AggregateClause clause)
				{
					this._clause = clause;
				}
				
				
				public AggregateParameter Producer_id_pk
				{
					get
					{
							AggregateParameter aggregate = new AggregateParameter(ColumnNames.Producer_id_pk, Parameters.Producer_id_pk);
							this._clause._entity.Query.AddAggregateParameter(aggregate);
							return aggregate;
					}
				}

				public AggregateParameter Producer_name
				{
					get
					{
							AggregateParameter aggregate = new AggregateParameter(ColumnNames.Producer_name, Parameters.Producer_name);
							this._clause._entity.Query.AddAggregateParameter(aggregate);
							return aggregate;
					}
				}

				public AggregateParameter Producer_sex
				{
					get
					{
							AggregateParameter aggregate = new AggregateParameter(ColumnNames.Producer_sex, Parameters.Producer_sex);
							this._clause._entity.Query.AddAggregateParameter(aggregate);
							return aggregate;
					}
				}

				public AggregateParameter Producer_dob
				{
					get
					{
							AggregateParameter aggregate = new AggregateParameter(ColumnNames.Producer_dob, Parameters.Producer_dob);
							this._clause._entity.Query.AddAggregateParameter(aggregate);
							return aggregate;
					}
				}

				public AggregateParameter Producer_bio
				{
					get
					{
							AggregateParameter aggregate = new AggregateParameter(ColumnNames.Producer_bio, Parameters.Producer_bio);
							this._clause._entity.Query.AddAggregateParameter(aggregate);
							return aggregate;
					}
				}


				private AggregateClause _clause;
			}
			#endregion
		
			public AggregateParameter Producer_id_pk
		    {
				get
		        {
					if(_Producer_id_pk_W == null)
	        	    {
						_Producer_id_pk_W = TearOff.Producer_id_pk;
					}
					return _Producer_id_pk_W;
				}
			}

			public AggregateParameter Producer_name
		    {
				get
		        {
					if(_Producer_name_W == null)
	        	    {
						_Producer_name_W = TearOff.Producer_name;
					}
					return _Producer_name_W;
				}
			}

			public AggregateParameter Producer_sex
		    {
				get
		        {
					if(_Producer_sex_W == null)
	        	    {
						_Producer_sex_W = TearOff.Producer_sex;
					}
					return _Producer_sex_W;
				}
			}

			public AggregateParameter Producer_dob
		    {
				get
		        {
					if(_Producer_dob_W == null)
	        	    {
						_Producer_dob_W = TearOff.Producer_dob;
					}
					return _Producer_dob_W;
				}
			}

			public AggregateParameter Producer_bio
		    {
				get
		        {
					if(_Producer_bio_W == null)
	        	    {
						_Producer_bio_W = TearOff.Producer_bio;
					}
					return _Producer_bio_W;
				}
			}

			private AggregateParameter _Producer_id_pk_W = null;
			private AggregateParameter _Producer_name_W = null;
			private AggregateParameter _Producer_sex_W = null;
			private AggregateParameter _Producer_dob_W = null;
			private AggregateParameter _Producer_bio_W = null;

			public void AggregateClauseReset()
			{
				_Producer_id_pk_W = null;
				_Producer_name_W = null;
				_Producer_sex_W = null;
				_Producer_dob_W = null;
				_Producer_bio_W = null;

				this._entity.Query.FlushAggregateParameters();

			}
	
			private BusinessEntity _entity;
			private TearOffAggregateParameter _tearOff;
			
		}
	
		public AggregateClause Aggregate
		{
			get
			{
				if(_aggregateClause == null)
				{
					_aggregateClause = new AggregateClause(this);
				}
		
				return _aggregateClause;
			}
		}
		
		private AggregateClause _aggregateClause = null;	
		#endregion
	
		protected override IDbCommand GetInsertCommand() 
		{
		
			SqlCommand cmd = new SqlCommand();
			cmd.CommandType = CommandType.StoredProcedure;
			cmd.CommandText = "[" + this.SchemaStoredProcedure + "proc_tblProducerInsert]";
	
			CreateParameters(cmd);
			
			SqlParameter p;
			p = cmd.Parameters[Parameters.Producer_id_pk.ParameterName];
			p.Direction = ParameterDirection.Output;
    
			return cmd;
		}
	
		protected override IDbCommand GetUpdateCommand()
		{
		
			SqlCommand cmd = new SqlCommand();
			cmd.CommandType = CommandType.StoredProcedure;
			cmd.CommandText = "[" + this.SchemaStoredProcedure + "proc_tblProducerUpdate]";
	
			CreateParameters(cmd);
			      
			return cmd;
		}
	
		protected override IDbCommand GetDeleteCommand()
		{
		
			SqlCommand cmd = new SqlCommand();
			cmd.CommandType = CommandType.StoredProcedure;
			cmd.CommandText = "[" + this.SchemaStoredProcedure + "proc_tblProducerDelete]";
	
			SqlParameter p;
			p = cmd.Parameters.Add(Parameters.Producer_id_pk);
			p.SourceColumn = ColumnNames.Producer_id_pk;
			p.SourceVersion = DataRowVersion.Current;

  
			return cmd;
		}
		
		private IDbCommand CreateParameters(SqlCommand cmd)
		{
			SqlParameter p;
		
			p = cmd.Parameters.Add(Parameters.Producer_id_pk);
			p.SourceColumn = ColumnNames.Producer_id_pk;
			p.SourceVersion = DataRowVersion.Current;

			p = cmd.Parameters.Add(Parameters.Producer_name);
			p.SourceColumn = ColumnNames.Producer_name;
			p.SourceVersion = DataRowVersion.Current;

			p = cmd.Parameters.Add(Parameters.Producer_sex);
			p.SourceColumn = ColumnNames.Producer_sex;
			p.SourceVersion = DataRowVersion.Current;

			p = cmd.Parameters.Add(Parameters.Producer_dob);
			p.SourceColumn = ColumnNames.Producer_dob;
			p.SourceVersion = DataRowVersion.Current;

			p = cmd.Parameters.Add(Parameters.Producer_bio);
			p.SourceColumn = ColumnNames.Producer_bio;
			p.SourceVersion = DataRowVersion.Current;


			return cmd;
		}
	}
}
