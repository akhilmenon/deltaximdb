
USE [deltaximdb]
GO

/****** Object:  StoredProcedure [proc_tblActorLoadByPrimaryKey]    Script Date: 30-May-18 19:10:43 ******/
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[proc_tblActorLoadByPrimaryKey]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
    DROP PROCEDURE [proc_tblActorLoadByPrimaryKey];
GO

CREATE PROCEDURE [proc_tblActorLoadByPrimaryKey]
(
	@actor_id_pk int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err int

	SELECT
		[actor_id_pk],
		[actor_name],
		[actor_sex],
		[actor_dob],
		[actor_bio]
	FROM [tblActor]
	WHERE
		([actor_id_pk] = @actor_id_pk)

	SET @Err = @@Error

	RETURN @Err
END
GO


-- Display the status of Proc creation
IF (@@Error = 0) PRINT 'Procedure Creation: proc_tblActorLoadByPrimaryKey Succeeded'
ELSE PRINT 'Procedure Creation: proc_tblActorLoadByPrimaryKey Error on Creation'
GO

/****** Object:  StoredProcedure [proc_tblActorLoadAll]    Script Date: 30-May-18 19:10:43 ******/
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[proc_tblActorLoadAll]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
    DROP PROCEDURE [proc_tblActorLoadAll];
GO

CREATE PROCEDURE [proc_tblActorLoadAll]
AS
BEGIN

	SET NOCOUNT ON
	DECLARE @Err int

	SELECT
		[actor_id_pk],
		[actor_name],
		[actor_sex],
		[actor_dob],
		[actor_bio]
	FROM [tblActor]

	SET @Err = @@Error

	RETURN @Err
END
GO


-- Display the status of Proc creation
IF (@@Error = 0) PRINT 'Procedure Creation: proc_tblActorLoadAll Succeeded'
ELSE PRINT 'Procedure Creation: proc_tblActorLoadAll Error on Creation'
GO

/****** Object:  StoredProcedure [proc_tblActorUpdate]    Script Date: 30-May-18 19:10:43 ******/
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[proc_tblActorUpdate]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
    DROP PROCEDURE [proc_tblActorUpdate];
GO

CREATE PROCEDURE [proc_tblActorUpdate]
(
	@actor_id_pk int,
	@actor_name nvarchar(50) = NULL,
	@actor_sex nvarchar(50) = NULL,
	@actor_dob nvarchar(150) = NULL,
	@actor_bio nvarchar(50) = NULL
)
AS
BEGIN

	SET NOCOUNT OFF
	DECLARE @Err int

	UPDATE [tblActor]
	SET
		[actor_name] = @actor_name,
		[actor_sex] = @actor_sex,
		[actor_dob] = @actor_dob,
		[actor_bio] = @actor_bio
	WHERE
		[actor_id_pk] = @actor_id_pk


	SET @Err = @@Error


	RETURN @Err
END
GO


-- Display the status of Proc creation
IF (@@Error = 0) PRINT 'Procedure Creation: proc_tblActorUpdate Succeeded'
ELSE PRINT 'Procedure Creation: proc_tblActorUpdate Error on Creation'
GO




/****** Object:  StoredProcedure [proc_tblActorInsert]    Script Date: 30-May-18 19:10:43 ******/
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[proc_tblActorInsert]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
    DROP PROCEDURE [proc_tblActorInsert];
GO

CREATE PROCEDURE [proc_tblActorInsert]
(
	@actor_id_pk int = NULL output,
	@actor_name nvarchar(50) = NULL,
	@actor_sex nvarchar(50) = NULL,
	@actor_dob nvarchar(150) = NULL,
	@actor_bio nvarchar(50) = NULL
)
AS
BEGIN

	SET NOCOUNT OFF
	DECLARE @Err int

	INSERT
	INTO [tblActor]
	(
		[actor_name],
		[actor_sex],
		[actor_dob],
		[actor_bio]
	)
	VALUES
	(
		@actor_name,
		@actor_sex,
		@actor_dob,
		@actor_bio
	)

	SET @Err = @@Error

	SELECT @actor_id_pk = SCOPE_IDENTITY()

	RETURN @Err
END
GO


-- Display the status of Proc creation
IF (@@Error = 0) PRINT 'Procedure Creation: proc_tblActorInsert Succeeded'
ELSE PRINT 'Procedure Creation: proc_tblActorInsert Error on Creation'
GO

/****** Object:  StoredProcedure [proc_tblActorDelete]    Script Date: 30-May-18 19:10:43 ******/
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[proc_tblActorDelete]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
    DROP PROCEDURE [proc_tblActorDelete];
GO

CREATE PROCEDURE [proc_tblActorDelete]
(
	@actor_id_pk int
)
AS
BEGIN

	SET NOCOUNT OFF
	DECLARE @Err int

	DELETE
	FROM [tblActor]
	WHERE
		[actor_id_pk] = @actor_id_pk
	SET @Err = @@Error

	RETURN @Err
END
GO


-- Display the status of Proc creation
IF (@@Error = 0) PRINT 'Procedure Creation: proc_tblActorDelete Succeeded'
ELSE PRINT 'Procedure Creation: proc_tblActorDelete Error on Creation'
GO
