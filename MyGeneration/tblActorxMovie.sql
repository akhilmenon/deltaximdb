
/****** Object:  StoredProcedure [proc_tblActorxMovieLoadByPrimaryKey]    Script Date: 30-May-18 19:10:43 ******/
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[proc_tblActorxMovieLoadByPrimaryKey]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
    DROP PROCEDURE [proc_tblActorxMovieLoadByPrimaryKey];
GO

CREATE PROCEDURE [proc_tblActorxMovieLoadByPrimaryKey]
(
	@actor_movie_id_pk int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err int

	SELECT
		[actor_movie_id_pk],
		[actor_id_fk],
		[movie_id_fk]
	FROM [tblActorxMovie]
	WHERE
		([actor_movie_id_pk] = @actor_movie_id_pk)

	SET @Err = @@Error

	RETURN @Err
END
GO


-- Display the status of Proc creation
IF (@@Error = 0) PRINT 'Procedure Creation: proc_tblActorxMovieLoadByPrimaryKey Succeeded'
ELSE PRINT 'Procedure Creation: proc_tblActorxMovieLoadByPrimaryKey Error on Creation'
GO

/****** Object:  StoredProcedure [proc_tblActorxMovieLoadAll]    Script Date: 30-May-18 19:10:43 ******/
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[proc_tblActorxMovieLoadAll]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
    DROP PROCEDURE [proc_tblActorxMovieLoadAll];
GO

CREATE PROCEDURE [proc_tblActorxMovieLoadAll]
AS
BEGIN

	SET NOCOUNT ON
	DECLARE @Err int

	SELECT
		[actor_movie_id_pk],
		[actor_id_fk],
		[movie_id_fk]
	FROM [tblActorxMovie]

	SET @Err = @@Error

	RETURN @Err
END
GO


-- Display the status of Proc creation
IF (@@Error = 0) PRINT 'Procedure Creation: proc_tblActorxMovieLoadAll Succeeded'
ELSE PRINT 'Procedure Creation: proc_tblActorxMovieLoadAll Error on Creation'
GO

/****** Object:  StoredProcedure [proc_tblActorxMovieUpdate]    Script Date: 30-May-18 19:10:43 ******/
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[proc_tblActorxMovieUpdate]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
    DROP PROCEDURE [proc_tblActorxMovieUpdate];
GO

CREATE PROCEDURE [proc_tblActorxMovieUpdate]
(
	@actor_movie_id_pk int,
	@actor_id_fk int = NULL,
	@movie_id_fk int = NULL
)
AS
BEGIN

	SET NOCOUNT OFF
	DECLARE @Err int

	UPDATE [tblActorxMovie]
	SET
		[actor_id_fk] = @actor_id_fk,
		[movie_id_fk] = @movie_id_fk
	WHERE
		[actor_movie_id_pk] = @actor_movie_id_pk


	SET @Err = @@Error


	RETURN @Err
END
GO


-- Display the status of Proc creation
IF (@@Error = 0) PRINT 'Procedure Creation: proc_tblActorxMovieUpdate Succeeded'
ELSE PRINT 'Procedure Creation: proc_tblActorxMovieUpdate Error on Creation'
GO




/****** Object:  StoredProcedure [proc_tblActorxMovieInsert]    Script Date: 30-May-18 19:10:43 ******/
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[proc_tblActorxMovieInsert]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
    DROP PROCEDURE [proc_tblActorxMovieInsert];
GO

CREATE PROCEDURE [proc_tblActorxMovieInsert]
(
	@actor_movie_id_pk int = NULL output,
	@actor_id_fk int = NULL,
	@movie_id_fk int = NULL
)
AS
BEGIN

	SET NOCOUNT OFF
	DECLARE @Err int

	INSERT
	INTO [tblActorxMovie]
	(
		[actor_id_fk],
		[movie_id_fk]
	)
	VALUES
	(
		@actor_id_fk,
		@movie_id_fk
	)

	SET @Err = @@Error

	SELECT @actor_movie_id_pk = SCOPE_IDENTITY()

	RETURN @Err
END
GO


-- Display the status of Proc creation
IF (@@Error = 0) PRINT 'Procedure Creation: proc_tblActorxMovieInsert Succeeded'
ELSE PRINT 'Procedure Creation: proc_tblActorxMovieInsert Error on Creation'
GO

/****** Object:  StoredProcedure [proc_tblActorxMovieDelete]    Script Date: 30-May-18 19:10:43 ******/
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[proc_tblActorxMovieDelete]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
    DROP PROCEDURE [proc_tblActorxMovieDelete];
GO

CREATE PROCEDURE [proc_tblActorxMovieDelete]
(
	@actor_movie_id_pk int
)
AS
BEGIN

	SET NOCOUNT OFF
	DECLARE @Err int

	DELETE
	FROM [tblActorxMovie]
	WHERE
		[actor_movie_id_pk] = @actor_movie_id_pk
	SET @Err = @@Error

	RETURN @Err
END
GO


-- Display the status of Proc creation
IF (@@Error = 0) PRINT 'Procedure Creation: proc_tblActorxMovieDelete Succeeded'
ELSE PRINT 'Procedure Creation: proc_tblActorxMovieDelete Error on Creation'
GO
