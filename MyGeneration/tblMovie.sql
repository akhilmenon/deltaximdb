
/****** Object:  StoredProcedure [proc_tblMovieLoadByPrimaryKey]    Script Date: 30-May-18 19:10:43 ******/
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[proc_tblMovieLoadByPrimaryKey]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
    DROP PROCEDURE [proc_tblMovieLoadByPrimaryKey];
GO

CREATE PROCEDURE [proc_tblMovieLoadByPrimaryKey]
(
	@movie_id_pk int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err int

	SELECT
		[movie_id_pk],
		[movie_name],
		[movie_release_year],
		[movie_plot],
		[movie_poster_image]
	FROM [tblMovie]
	WHERE
		([movie_id_pk] = @movie_id_pk)

	SET @Err = @@Error

	RETURN @Err
END
GO


-- Display the status of Proc creation
IF (@@Error = 0) PRINT 'Procedure Creation: proc_tblMovieLoadByPrimaryKey Succeeded'
ELSE PRINT 'Procedure Creation: proc_tblMovieLoadByPrimaryKey Error on Creation'
GO

/****** Object:  StoredProcedure [proc_tblMovieLoadAll]    Script Date: 30-May-18 19:10:43 ******/
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[proc_tblMovieLoadAll]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
    DROP PROCEDURE [proc_tblMovieLoadAll];
GO

CREATE PROCEDURE [proc_tblMovieLoadAll]
AS
BEGIN

	SET NOCOUNT ON
	DECLARE @Err int

	SELECT
		[movie_id_pk],
		[movie_name],
		[movie_release_year],
		[movie_plot],
		[movie_poster_image]
	FROM [tblMovie]

	SET @Err = @@Error

	RETURN @Err
END
GO


-- Display the status of Proc creation
IF (@@Error = 0) PRINT 'Procedure Creation: proc_tblMovieLoadAll Succeeded'
ELSE PRINT 'Procedure Creation: proc_tblMovieLoadAll Error on Creation'
GO

/****** Object:  StoredProcedure [proc_tblMovieUpdate]    Script Date: 30-May-18 19:10:43 ******/
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[proc_tblMovieUpdate]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
    DROP PROCEDURE [proc_tblMovieUpdate];
GO

CREATE PROCEDURE [proc_tblMovieUpdate]
(
	@movie_id_pk int,
	@movie_name nvarchar(50) = NULL,
	@movie_release_year nvarchar(50) = NULL,
	@movie_plot nvarchar(50) = NULL,
	@movie_poster_image nvarchar(150) = NULL
)
AS
BEGIN

	SET NOCOUNT OFF
	DECLARE @Err int

	UPDATE [tblMovie]
	SET
		[movie_name] = @movie_name,
		[movie_release_year] = @movie_release_year,
		[movie_plot] = @movie_plot,
		[movie_poster_image] = @movie_poster_image
	WHERE
		[movie_id_pk] = @movie_id_pk


	SET @Err = @@Error


	RETURN @Err
END
GO


-- Display the status of Proc creation
IF (@@Error = 0) PRINT 'Procedure Creation: proc_tblMovieUpdate Succeeded'
ELSE PRINT 'Procedure Creation: proc_tblMovieUpdate Error on Creation'
GO




/****** Object:  StoredProcedure [proc_tblMovieInsert]    Script Date: 30-May-18 19:10:43 ******/
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[proc_tblMovieInsert]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
    DROP PROCEDURE [proc_tblMovieInsert];
GO

CREATE PROCEDURE [proc_tblMovieInsert]
(
	@movie_id_pk int = NULL output,
	@movie_name nvarchar(50) = NULL,
	@movie_release_year nvarchar(50) = NULL,
	@movie_plot nvarchar(50) = NULL,
	@movie_poster_image nvarchar(150) = NULL
)
AS
BEGIN

	SET NOCOUNT OFF
	DECLARE @Err int

	INSERT
	INTO [tblMovie]
	(
		[movie_name],
		[movie_release_year],
		[movie_plot],
		[movie_poster_image]
	)
	VALUES
	(
		@movie_name,
		@movie_release_year,
		@movie_plot,
		@movie_poster_image
	)

	SET @Err = @@Error

	SELECT @movie_id_pk = SCOPE_IDENTITY()

	RETURN @Err
END
GO


-- Display the status of Proc creation
IF (@@Error = 0) PRINT 'Procedure Creation: proc_tblMovieInsert Succeeded'
ELSE PRINT 'Procedure Creation: proc_tblMovieInsert Error on Creation'
GO

/****** Object:  StoredProcedure [proc_tblMovieDelete]    Script Date: 30-May-18 19:10:43 ******/
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[proc_tblMovieDelete]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
    DROP PROCEDURE [proc_tblMovieDelete];
GO

CREATE PROCEDURE [proc_tblMovieDelete]
(
	@movie_id_pk int
)
AS
BEGIN

	SET NOCOUNT OFF
	DECLARE @Err int

	DELETE
	FROM [tblMovie]
	WHERE
		[movie_id_pk] = @movie_id_pk
	SET @Err = @@Error

	RETURN @Err
END
GO


-- Display the status of Proc creation
IF (@@Error = 0) PRINT 'Procedure Creation: proc_tblMovieDelete Succeeded'
ELSE PRINT 'Procedure Creation: proc_tblMovieDelete Error on Creation'
GO
