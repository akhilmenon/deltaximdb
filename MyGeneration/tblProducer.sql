
/****** Object:  StoredProcedure [proc_tblProducerLoadByPrimaryKey]    Script Date: 30-May-18 19:10:43 ******/
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[proc_tblProducerLoadByPrimaryKey]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
    DROP PROCEDURE [proc_tblProducerLoadByPrimaryKey];
GO

CREATE PROCEDURE [proc_tblProducerLoadByPrimaryKey]
(
	@producer_id_pk int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err int

	SELECT
		[producer_id_pk],
		[producer_name],
		[producer_sex],
		[producer_dob],
		[producer_bio]
	FROM [tblProducer]
	WHERE
		([producer_id_pk] = @producer_id_pk)

	SET @Err = @@Error

	RETURN @Err
END
GO


-- Display the status of Proc creation
IF (@@Error = 0) PRINT 'Procedure Creation: proc_tblProducerLoadByPrimaryKey Succeeded'
ELSE PRINT 'Procedure Creation: proc_tblProducerLoadByPrimaryKey Error on Creation'
GO

/****** Object:  StoredProcedure [proc_tblProducerLoadAll]    Script Date: 30-May-18 19:10:43 ******/
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[proc_tblProducerLoadAll]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
    DROP PROCEDURE [proc_tblProducerLoadAll];
GO

CREATE PROCEDURE [proc_tblProducerLoadAll]
AS
BEGIN

	SET NOCOUNT ON
	DECLARE @Err int

	SELECT
		[producer_id_pk],
		[producer_name],
		[producer_sex],
		[producer_dob],
		[producer_bio]
	FROM [tblProducer]

	SET @Err = @@Error

	RETURN @Err
END
GO


-- Display the status of Proc creation
IF (@@Error = 0) PRINT 'Procedure Creation: proc_tblProducerLoadAll Succeeded'
ELSE PRINT 'Procedure Creation: proc_tblProducerLoadAll Error on Creation'
GO

/****** Object:  StoredProcedure [proc_tblProducerUpdate]    Script Date: 30-May-18 19:10:43 ******/
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[proc_tblProducerUpdate]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
    DROP PROCEDURE [proc_tblProducerUpdate];
GO

CREATE PROCEDURE [proc_tblProducerUpdate]
(
	@producer_id_pk int,
	@producer_name nvarchar(50) = NULL,
	@producer_sex nvarchar(50) = NULL,
	@producer_dob nvarchar(50) = NULL,
	@producer_bio nchar(10) = NULL
)
AS
BEGIN

	SET NOCOUNT OFF
	DECLARE @Err int

	UPDATE [tblProducer]
	SET
		[producer_name] = @producer_name,
		[producer_sex] = @producer_sex,
		[producer_dob] = @producer_dob,
		[producer_bio] = @producer_bio
	WHERE
		[producer_id_pk] = @producer_id_pk


	SET @Err = @@Error


	RETURN @Err
END
GO


-- Display the status of Proc creation
IF (@@Error = 0) PRINT 'Procedure Creation: proc_tblProducerUpdate Succeeded'
ELSE PRINT 'Procedure Creation: proc_tblProducerUpdate Error on Creation'
GO




/****** Object:  StoredProcedure [proc_tblProducerInsert]    Script Date: 30-May-18 19:10:43 ******/
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[proc_tblProducerInsert]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
    DROP PROCEDURE [proc_tblProducerInsert];
GO

CREATE PROCEDURE [proc_tblProducerInsert]
(
	@producer_id_pk int = NULL output,
	@producer_name nvarchar(50) = NULL,
	@producer_sex nvarchar(50) = NULL,
	@producer_dob nvarchar(50) = NULL,
	@producer_bio nchar(10) = NULL
)
AS
BEGIN

	SET NOCOUNT OFF
	DECLARE @Err int

	INSERT
	INTO [tblProducer]
	(
		[producer_name],
		[producer_sex],
		[producer_dob],
		[producer_bio]
	)
	VALUES
	(
		@producer_name,
		@producer_sex,
		@producer_dob,
		@producer_bio
	)

	SET @Err = @@Error

	SELECT @producer_id_pk = SCOPE_IDENTITY()

	RETURN @Err
END
GO


-- Display the status of Proc creation
IF (@@Error = 0) PRINT 'Procedure Creation: proc_tblProducerInsert Succeeded'
ELSE PRINT 'Procedure Creation: proc_tblProducerInsert Error on Creation'
GO

/****** Object:  StoredProcedure [proc_tblProducerDelete]    Script Date: 30-May-18 19:10:43 ******/
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[proc_tblProducerDelete]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
    DROP PROCEDURE [proc_tblProducerDelete];
GO

CREATE PROCEDURE [proc_tblProducerDelete]
(
	@producer_id_pk int
)
AS
BEGIN

	SET NOCOUNT OFF
	DECLARE @Err int

	DELETE
	FROM [tblProducer]
	WHERE
		[producer_id_pk] = @producer_id_pk
	SET @Err = @@Error

	RETURN @Err
END
GO


-- Display the status of Proc creation
IF (@@Error = 0) PRINT 'Procedure Creation: proc_tblProducerDelete Succeeded'
ELSE PRINT 'Procedure Creation: proc_tblProducerDelete Error on Creation'
GO
