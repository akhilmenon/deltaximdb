
/****** Object:  StoredProcedure [proc_tblProducerxMovieLoadByPrimaryKey]    Script Date: 30-May-18 19:10:43 ******/
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[proc_tblProducerxMovieLoadByPrimaryKey]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
    DROP PROCEDURE [proc_tblProducerxMovieLoadByPrimaryKey];
GO

CREATE PROCEDURE [proc_tblProducerxMovieLoadByPrimaryKey]
(
	@producer_movie_id_pk int
)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @Err int

	SELECT
		[producer_movie_id_pk],
		[producer_id_fk],
		[movie_id_fk]
	FROM [tblProducerxMovie]
	WHERE
		([producer_movie_id_pk] = @producer_movie_id_pk)

	SET @Err = @@Error

	RETURN @Err
END
GO


-- Display the status of Proc creation
IF (@@Error = 0) PRINT 'Procedure Creation: proc_tblProducerxMovieLoadByPrimaryKey Succeeded'
ELSE PRINT 'Procedure Creation: proc_tblProducerxMovieLoadByPrimaryKey Error on Creation'
GO

/****** Object:  StoredProcedure [proc_tblProducerxMovieLoadAll]    Script Date: 30-May-18 19:10:43 ******/
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[proc_tblProducerxMovieLoadAll]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
    DROP PROCEDURE [proc_tblProducerxMovieLoadAll];
GO

CREATE PROCEDURE [proc_tblProducerxMovieLoadAll]
AS
BEGIN

	SET NOCOUNT ON
	DECLARE @Err int

	SELECT
		[producer_movie_id_pk],
		[producer_id_fk],
		[movie_id_fk]
	FROM [tblProducerxMovie]

	SET @Err = @@Error

	RETURN @Err
END
GO


-- Display the status of Proc creation
IF (@@Error = 0) PRINT 'Procedure Creation: proc_tblProducerxMovieLoadAll Succeeded'
ELSE PRINT 'Procedure Creation: proc_tblProducerxMovieLoadAll Error on Creation'
GO

/****** Object:  StoredProcedure [proc_tblProducerxMovieUpdate]    Script Date: 30-May-18 19:10:43 ******/
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[proc_tblProducerxMovieUpdate]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
    DROP PROCEDURE [proc_tblProducerxMovieUpdate];
GO

CREATE PROCEDURE [proc_tblProducerxMovieUpdate]
(
	@producer_movie_id_pk int,
	@producer_id_fk int = NULL,
	@movie_id_fk int = NULL
)
AS
BEGIN

	SET NOCOUNT OFF
	DECLARE @Err int

	UPDATE [tblProducerxMovie]
	SET
		[producer_id_fk] = @producer_id_fk,
		[movie_id_fk] = @movie_id_fk
	WHERE
		[producer_movie_id_pk] = @producer_movie_id_pk


	SET @Err = @@Error


	RETURN @Err
END
GO


-- Display the status of Proc creation
IF (@@Error = 0) PRINT 'Procedure Creation: proc_tblProducerxMovieUpdate Succeeded'
ELSE PRINT 'Procedure Creation: proc_tblProducerxMovieUpdate Error on Creation'
GO




/****** Object:  StoredProcedure [proc_tblProducerxMovieInsert]    Script Date: 30-May-18 19:10:43 ******/
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[proc_tblProducerxMovieInsert]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
    DROP PROCEDURE [proc_tblProducerxMovieInsert];
GO

CREATE PROCEDURE [proc_tblProducerxMovieInsert]
(
	@producer_movie_id_pk int = NULL output,
	@producer_id_fk int = NULL,
	@movie_id_fk int = NULL
)
AS
BEGIN

	SET NOCOUNT OFF
	DECLARE @Err int

	INSERT
	INTO [tblProducerxMovie]
	(
		[producer_id_fk],
		[movie_id_fk]
	)
	VALUES
	(
		@producer_id_fk,
		@movie_id_fk
	)

	SET @Err = @@Error

	SELECT @producer_movie_id_pk = SCOPE_IDENTITY()

	RETURN @Err
END
GO


-- Display the status of Proc creation
IF (@@Error = 0) PRINT 'Procedure Creation: proc_tblProducerxMovieInsert Succeeded'
ELSE PRINT 'Procedure Creation: proc_tblProducerxMovieInsert Error on Creation'
GO

/****** Object:  StoredProcedure [proc_tblProducerxMovieDelete]    Script Date: 30-May-18 19:10:43 ******/
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[proc_tblProducerxMovieDelete]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
    DROP PROCEDURE [proc_tblProducerxMovieDelete];
GO

CREATE PROCEDURE [proc_tblProducerxMovieDelete]
(
	@producer_movie_id_pk int
)
AS
BEGIN

	SET NOCOUNT OFF
	DECLARE @Err int

	DELETE
	FROM [tblProducerxMovie]
	WHERE
		[producer_movie_id_pk] = @producer_movie_id_pk
	SET @Err = @@Error

	RETURN @Err
END
GO


-- Display the status of Proc creation
IF (@@Error = 0) PRINT 'Procedure Creation: proc_tblProducerxMovieDelete Succeeded'
ELSE PRINT 'Procedure Creation: proc_tblProducerxMovieDelete Error on Creation'
GO
